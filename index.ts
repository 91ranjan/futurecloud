/**
 * 1. Check if the db is up
 * 2. Check if kafka is reachable
 * 3. Check if cron job is up
 * 4. Start all services
 * 5. Start all modules
 * 6. Start web server
 */

import * as mongoose from 'mongoose';
import * as bluebird from 'bluebird';
import config from './config/dev';
import * as logger from 'tracer';
import * as pm2 from 'pm2';
const _console = logger.colorConsole();

(async function() {

    // 1. Checking if db is up or not
    (<any>mongoose).Promise = bluebird;
    mongoose.connect(config.dbUrl, { useNewUrlParser: true, useUnifiedTopology: true });
    const db = mongoose.connection;
    db.on('error', console.error.bind(console, 'connection error:'));
    db.once('open', function () {
        _console.log('Database is up and running');
    });
    // 2. Check if kafka is up ot not.
    // 3. Check if cron job service is up or not.
    // 4. Starting microservices
    pm2.connect(() => {
        pm2.list(function(err, processes) {
            pm2.launchBus((err, bus) => {
                // Starting service manager
                const isServiceManagerRunning = processes.some(_process => _process.name === "servicemanager" && 
                    _process.pm2_env.status === 'online')
                if(!isServiceManagerRunning) {
                    pm2.start({
                        name: "servicemanager",
                        script: 'ts-node --project tsconfig.json -- ./microservices/servicemanager/index.ts',
                        max_memory_restart: "20M",
                        watch: true
                    }, (err, apps) => {
                        if(err) {
                            console.error(err)
                        } else {
                            console.log('Service manager running.');
                        }
                    })
                } else {
                    console.log("Service manger already running.")
                }
                // Starting module manager
                bus.on('error', console.error);
            });
        })
    })
})();