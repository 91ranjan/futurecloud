export const LogLevel = {
    Info: 1,
    Debug: 2,
    Warning: 3,
    Error: 4,
    Fatal: 5,
};

export const LogType = {
    Core: 1,
    Service: 2,
    Modules: 3,
};

export const LogRequestMethod = {
    GET: 'GET',
    POST: 'POST',
    PUT: 'PUT',
    DELETE: 'DELETE',
    WEB_SOCKET: 'WEB_SOCKET',
};
