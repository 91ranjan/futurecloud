import * as logger from 'tracer';
import services from '../../../services';

const scanMusic = services.music.controllers.list.scanMusic;
const _console = logger.colorConsole();

export async function musicScannerJob() {
    await services.scheduler.controllers.schedule.start();
    // await scanMusic();
    await services.scheduler.controllers.schedule.define(
        'scan audio',
        async () => {
            await scanMusic();
            _console.debug('Scanning music...');
        },
        {}
    );
    await services.scheduler.controllers.schedule.every('5 minutes', 'scan audio');
    // Check if the job is there to scan music
}
