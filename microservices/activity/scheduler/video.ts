import * as logger from 'tracer';
import services from '../../../services';

const scanVideo = services.video.controllers.list.scanVideo;
const _console = logger.colorConsole();

export async function videoScannerJob() {
    await services.scheduler.controllers.schedule.start();
    // await scanMusic();
    await services.scheduler.controllers.schedule.define(
        'scan video',
        async () => {
            await scanVideo();
            _console.debug('Scanning video...');
        },
        {}
    );
    await services.scheduler.controllers.schedule.every('5 minutes', 'scan video');
    // Check if the job is there to scan video
}
