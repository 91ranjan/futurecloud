import * as logger from 'tracer';

import { ActionTypes } from '../topics';
import config from '../config';
import { createTopicName } from '../../../common/createTopics';

import services from '../../../services';

const _console = logger.colorConsole();

export async function messageCtrl(request, response) {
    const { value } = request;
    const payload = JSON.parse(value);
    services.database.activity.logs.create(payload);
}
