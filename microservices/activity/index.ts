import * as logger from 'tracer';
import { messageCtrl } from './controller/messageCtrl';
import { musicScannerJob } from './scheduler/music';
import { videoScannerJob } from './scheduler/video';
import topics from './topics';

const _console = logger.colorConsole();

(async function () {
    await musicScannerJob();
    await videoScannerJob();
})();
