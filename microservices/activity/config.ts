import config from "../config";

export default {
    name: 'Activity',
    baseTopic: 'activity',
    app_key: 'activity',
    port: config.activity.port
};
