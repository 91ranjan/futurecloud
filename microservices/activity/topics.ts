import { createMicroServiceTopics, createTopicName } from '../../common/createTopics';
import { keyMirror } from '../../common/utils/keymirror';
import Config from './config';

const serviceTopics = createMicroServiceTopics(Config.baseTopic);
export const ActionTypes = {
    LOG: 'LOG',
};

export default {
    topics: [
        {
            topic: 'activity',
            partitions: 1,
            replicationFactor: 1,
        },
    ],
};
