import * as logger from 'tracer';
import fastify from 'fastify';
const fs = require('fs');
const path = require('path');
import * as qs from 'querystring';
import serverConfig from '../../config/dev';
const fastifyStatic = require('fastify-static');

import config from './config';

import { LogLevel, LogType } from '../activity/constant/messageConstants';
// import { logActivity } from '../activity/logger';

const _console = logger.colorConsole();
function getProdConfig() {
    return {
        logger: true,
        https: {
            key: fs.readFileSync(path.join(__dirname, '../../config/privkey.pem')),
            cert: fs.readFileSync(path.join(__dirname, '../../config/fullchain.pem')),
        },
    };
}
const server = fastify(
    serverConfig.environment === 'production'
        ? getProdConfig()
        : {
              logger: true,
          }
);

// HTTP
server.register(require('fastify-cors'), (instance) => (req, callback) => {
    let corsOptions = { origin: true };
    callback(null, corsOptions); // callback expects two parameters: error and options
});
server.register(fastifyStatic, {
    root: serverConfig.publicpath,
    prefix: '/public/', // optional: default '/'
});
server.register(fastifyStatic, {
    root: serverConfig.assetspath,
    prefix: '/assets/', // optional: default '/'
    decorateReply: false,
});
server.register(require('fastify-multipart'));
server.register(require('fastify-formbody'), { parser: (str) => qs.parse(str) });
server.register(require('./routes/apps.route'), { prefix: '/apps' });
server.register(require('./routes/system/index'), { prefix: '/system' });
server.register(require('./routes/settings/index.route'), { prefix: '/settings' });

server.listen(config.port, '0.0.0.0', function (err, address) {
    if (err) {
        server.log.error(err);
        process.exit(1);
    }
    /*logActivity({
        app: 'web',
        level: LogLevel.Info,
        type: LogType.Modules,
        title: 'Http server bootup',
        messages: {
            customMessage: `${config.name} server listening on ${address}`,
        },
    });*/
    server.log.info(`${config.name} server listening on ${address}`);
});
