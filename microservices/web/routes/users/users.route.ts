import * as logger from 'tracer';
import services from '../../../../services';
import { LogLevel, LogType } from '../../../activity/constant/messageConstants';
import { logActivity } from '../../../activity/logger';
import { massageItemResponse } from '../../helpers/responseHelper';
const _console = logger.colorConsole();

export default function (fastify, opts, done) {
    fastify.post('/me', async (request, reply) => {
        try {
            const user = await services.users.controllers.users.getById(request.user.id);
            return massageItemResponse(user);
        } catch (e) {
            reply.status(400).send({ message: e.message });
        }
    });
    fastify.put('/update', async (request, reply) => {
        try {
            const user = request.user;
            let updatedUser;
            if (user.role === 1) {
                // Admin can update all the users.
                updatedUser = await services.users.controllers.users.update(request.body);
            } else if (user.id === request.body.id) {
                // User can only update its own user.
                updatedUser = await services.users.controllers.users.update(request.body);
            }
            return massageItemResponse(updatedUser);
        } catch (e) {
            reply.status(400).send({ message: e.message });
        }
    });
    done();
}
