import * as logger from 'tracer';

export default function (fastify, opts, done) {
    fastify.register(require('./users.route'), { prefix: '/' });
    done();
}
