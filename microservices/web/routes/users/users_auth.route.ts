import * as logger from 'tracer';
import services from '../../../../services';
import { LogLevel, LogType } from '../../../activity/constant/messageConstants';
import { logActivity } from '../../../activity/logger';
import { massageItemResponse } from '../../helpers/responseHelper';
const _console = logger.colorConsole();

export default function (fastify, opts, done) {
    fastify.post('/login', async (request, reply) => {
        try {
            const response = await services.users.controllers.auth.login({
                username: request.body.username,
                email: request.body.email,
                password: request.body.password,
            });
            return response;
        } catch (e) {
            reply.status(400).send({ message: e.message });
        }
    });
    fastify.post('/signup', async (request, reply) => {
        try {
            return await services.users.controllers.auth.signup(request.body);
        } catch (e) {
            reply.status(400).send({ message: e.message });
        }
    });
    done();
}
