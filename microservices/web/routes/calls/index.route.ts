import * as logger from 'tracer';
import { v4 as uuidv4 } from 'uuid';
import services from '../../../../services';
import { massageItemResponse, massageListResponse } from '../../helpers/responseHelper';
import serverConfig from '../../../../config/dev';
import * as fs from 'fs-extra';
import { asyncForEach } from '../../../../common/utils/asyncHelpers';
const _console = logger.colorConsole();
const pathFunc = require('path');

let users = [],
    rooms = {};

async function getUserRooms(user) {
    return await services.conference.controllers.rooms.getAll({
        $or: [
            {
                "connection.client1" : user
            },
            {
                "connection.client2" : user
            },
        ]
    });
}

function emitToAllParticipants(socket, room, type, payload) {
     // Updating all the other users.
     room.users.forEach(_user => {
        const remoteUser = findUser(_user.username);
        if (remoteUser) {
            remoteUser && socket.to(remoteUser.socketId).emit(type, payload);
        } else {
            _console.error(`Could not find the user ${_user.username}`)
        }
    })
}

function findUser(username) {
    return users.find(_user => _user.username === username)
}
function findUserBySocket(socketId) {
    return users.find(_user => _user.socketId === socketId)
}
function removeUser(socketId) {
    users = users.filter(_user => _user.socketId !== socketId)
}
function getUserList(includeGuests = false) {
    return users.filter(_user => includeGuests ? true : !_user.isGuest ).map(_user => ({username: _user.username, id: _user.id, socketId: _user.socketId}))
}

async function leaveRoom(socket, roomId, user, io) {
    if (user) {
        let room = await services.conference.controllers.rooms.getByRoomId(roomId);
        room = room.toJSON();
        
        // 1. Removing the user connection
        room.connections = room.connections.filter(function(_conn: any) {
            if (_conn.client1 !== user && _conn.client2 !== user) {
                return true;
            }
            return false;
        });
        
        // 2. Removing the user from the list of users in the room
        room.users = room.users.filter(_user => {
            if(_user.username !== user) {
                return true;
            }
            return false;
        });
        // Updating all the other users.
        emitToAllParticipants(socket, room, 'user_left', {
            user
        })

        socket.to(roomId).emit('user_left', {
            user
        })
        
        if (room.host === user && room.type==='onetimecall') {
            socket.to(roomId).emit('end_call', {
                message: 'Host has left the meeting.'
            });
            await services.conference.controllers.rooms.del({id: room.id})
        }  else if (!room.users.length && room.type==='onetimecall') {
            // 3. Delete the room if it was a one time call and there are no users in the room.
            await services.conference.controllers.rooms.del({id: room.id})
        } else {
            await services.conference.controllers.rooms.update(room);
        }
        socket.leave(roomId);
    } else {
        console.error(`Could not find the user to remove from the room ${roomId}`)
    }
}

export default function (fastify, opts, done) {
    fastify.register(require('fastify-socket.io'), {
        path: "/apps/calls/socket"
    });
    fastify.addHook('onReady', function (done) {
        const io = (<any>fastify).io;
        io.on('connection', async (socket) => { 
            socket.on('initialize', data => {
                let user = findUser(data.username);
                if (!user) {
                    user = {
                        conn: socket,
                        username: data.username,
                        socketId: socket.id,
                        isGuest: data.isGuest
                    };
                    users.push(user);
                } else {
                    user.socketId = socket.id
                }
                socket.username = data.username;
                socket.emit("update_user_list", {users: getUserList()})
                socket.broadcast.emit("update_user_list", {
                    users: getUserList()
                  });
            })
            socket.on('disconnect', async () => {
                console.info('Disconnecting user');
                const user = findUserBySocket(socket.id);
                if (user){
                    const roomIds = await getUserRooms(user.username);
                    removeUser(socket.id)
                    await asyncForEach(roomIds, async (_roomId) => {
                        await leaveRoom(socket, _roomId, user.username, io)

                    })
                    socket.broadcast.emit("update_user_list", {
                        users: getUserList()
                      });
                }
                //TODO: reject any incomming calls.
            })
            socket.on('leave_room', async (data) => {
                const {roomId, user} = data;
                await leaveRoom(socket, roomId, user, io);
            })

            // From client 1
            // This is a one to one call and the room will be deleted once there are no users.
            socket.on('initiate_call', async (message) => {
                const initiateCaller = findUserBySocket(socket.id);
                
                // 1. Create a new room
                const room = await services.conference.controllers.rooms.create({
                    name: '',
                    host: initiateCaller.username,
                    connections: [
                        {
                            client1: initiateCaller.username,
                            status: "call_initiated",
                            client2: message.remote_client,
                        }
                    ],
                    users: [{
                        username: initiateCaller.username,
                        isGuest: false
                    }],
                    type: message.type || "onetimecall"
                })
                _console.log(`Client 1 Joining room ${room.roomId}`);
                // 2. Add a room in the socket
                socket.join(room.roomId);

                // 3. Send a request to the user being called.
                const toUser = findUser(message.remote_client);
                socket.to(toUser.socketId).emit("call_made", {
                    from_client: initiateCaller.username,
                    roomId: room.roomId,
                  });
            });

            socket.on('answer_made', async(data) => {
                const {roomId, answer, to_client} = data;
                const fromClient = findUserBySocket(socket.id);
                const remoteClient = findUser(to_client);
                 // 1. update the room connection status
                let room = await services.conference.controllers.rooms.getByRoomId(roomId);
                room = room.toJSON();
                room.connections = room.connections.map(_connection => {
                    // Find the connection in the room for the users in the transaction
                    if (_connection.client1 === fromClient.username && _connection.client2 === remoteClient.username) {
                        _connection.status = "answer_made";
                    }
                    return _connection;
                })
                // 2. Update the room
                await services.conference.controllers.rooms.update(room);

                // 3. Send the answer to the client 2
                socket.to(remoteClient.socketId).emit("answer_made", {
                    from_client: fromClient.username,
                    answer,
                    roomId
                });
            })
            
            // From client 2
            socket.on('send_offer', async (data) => {
                const {roomId, to_client, offer} = data;
                const fromClient = findUserBySocket(socket.id);
                const remoteClient = findUser(to_client);
                
                // 1. update the room connection status
                let room = await services.conference.controllers.rooms.getByRoomId(roomId);
                room = room.toJSON();
                room.connections = room.connections.map(_connection => {
                    // Find the connection in the room for the users in the transaction
                    if (_connection.client1 === remoteClient.username && _connection.client2 === fromClient.username) {
                        _connection.status = "offer_sent";
                    }
                    return _connection;
                })
                
                // 2.  Add the user to the room users
                if(!room.users.some(_user => _user.username === fromClient.username)) {
                    room.users.length && room.users.push({ username:  fromClient.username, isGuest: false})
                }
                
                // 3.  Update the room
                await services.conference.controllers.rooms.update(room);

                // 4. Client 2 joins the socket room
                socket.join(roomId);

                _console.log(`Client 2 Joining room ${roomId}`)
                
                // 4. Send this offer to the client 1
                socket.to(remoteClient.socketId).emit("offer_made", {
                    from_client: fromClient.username,
                    offer: offer,
                    roomId
                  });
            })

            socket.on('reject_call', async (data) => {
                const {from_client, roomId} = data;
                const client1 = findUser(from_client);
                const client2 = findUserBySocket(socket.id);
                // 1. Removing the connection entry
                let room = await services.conference.controllers.rooms.getByRoomId(roomId);
                room = room.toJSON();
                room.connections = room.connections.filter(_conn => {
                    return _conn.client1 !== from_client && _conn.client2 === client2.username
                })
                // 2. Removing the users entry
                room.users = room.users.filter(_user => _user.username !== client2.username)
                // 3. Update the room.
                await services.conference.controllers.rooms.update(room);

                // 4. Send the status to the caller
                socket.to(client1.socketId).emit('call_rejected', {
                    roomId,
                    to_client: client2.username
                })
            })

            // Other clients
            socket.on('join_meeting', async (data) => {
                const {roomId, from_client} = data;
                const fromUser = findUserBySocket(socket.id);

                // 1. Add the new client to the room
                socket.join(roomId);
                // 2. Call all the users in the room
                let room = await services.conference.controllers.rooms.getByRoomId(roomId);
                if (room && fromUser) {
                    room = room.toJSON();
    
                    room.users.forEach(_user => {
                        // Check if the caller is by mistake not removed from the room.
                        if (_user.username === fromUser.username) {
                            return;
                        }
                        const remoteUser = findUser(_user.username);
                        if (remoteUser) {
                            socket.to(remoteUser.socketId).emit("call_made", {
                                from_client,
                                roomId,
                              });
                            //3. Add the connection from this user to every other user in the room
                            room.connections.push({
                                client1: from_client,
                                status: "call_initiated",
                                client2: _user.username,
                            })
                        }
                    })
                    const otherUsers = room.users.filter(_user => _user.username !== fromUser.username);
                    //4. Add this user to the room users
                    // Checking if user already not in the group
                    otherUsers.length === room.users.length && room.users.push({
                        username: from_client,
                        isGuest: false
                    });
    
                    // 5. Update the room
                    await services.conference.controllers.rooms.update(room);
    
                    // 6. Make an empty offer to the user to join the room.
                    if (!otherUsers.length) {
                        socket.emit("offer_made", {
                            roomId
                        });
                    }
                } else {
                    socket.emit("room_error", {
                        message: `Please check the room you are trying to connect to.`,
                        data
                    });                
                }
            })

            // Room releated listeners
            socket.on('share_screen', async (data) => {
                const {roomId, from_client} = data;
                const fromUser = findUserBySocket(socket.id);
                let room = await services.conference.controllers.rooms.getByRoomId(roomId);
                room = room.toJSON();

                // Add the user to scrren share in the rooom 
                if (room.screen_share) {
                    if (room.screen_share.username) {

                    } else {
                        room.screen_share.username = fromUser.username
                    }
                }
                // Update the room
                await services.conference.controllers.rooms.update(room);
                
                // Send the update to all the clients.
                emitToAllParticipants(socket, room, 'screen_shared', {
                    from_client
                })
                socket.emit('screen_shared', {
                    from_client
                })
            })
            
            // common listner for both clients
            socket.on('add_candidate', (data) => {
                const {candidate, to_client} = data;
                const toUser = findUser(to_client);
                const fromUser = findUserBySocket(socket.id);
                socket.to(toUser.socketId).emit("add_candidate", {
                    from_client: fromUser.username,
                    candidate: candidate
                  });
            })

            socket.on('send_group_chate_message', async (data) => {
                const { type, from_client, to_client, roomId, text } = data;
                let room = await services.conference.controllers.rooms.getByRoomId(roomId);
                room = room.toJSON();

                room.users.forEach(_user => {
                    const remoteUser = findUser(_user.username);
                    if (remoteUser) {
                        socket.to(remoteUser.socketId).emit("group_chat_message", {
                            message: {
                                type, text, from_client
                            }
                          });
                    }
                })
                socket.emit('group_chat_message', {message: {
                    type, text, from_client
                }})
            })

        });
        done()
      });
      fastify.register(require('./rooms.route'), { prefix: '/rooms' });
    done();
}
