import * as logger from 'tracer';

export default function (fastify, opts, done) {
    // fastify.post('/preferences', async (request, reply)
    fastify.register(require('./journal_books.route'), { prefix: '/books' });
    fastify.register(require('./journal_entries.route'), { prefix: '/entries' });
    done();
}
