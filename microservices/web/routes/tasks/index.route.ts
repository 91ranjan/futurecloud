import * as logger from 'tracer';

export default function (fastify, opts, done) {
    // fastify.post('/preferences', async (request, reply)
    fastify.register(require('./tasksbucket.route'), { prefix: '/buckets' });
    fastify.register(require('./tasks.route'), { prefix: '/tasks' });
    fastify.register(require('./board_tasks.route'), { prefix: '/boards_tasks' });
    fastify.register(require('./tasks_log.route'), { prefix: '/logs' });
    fastify.register(require('./task_blocks.route'), { prefix: '/blocks' });
    done();
}
