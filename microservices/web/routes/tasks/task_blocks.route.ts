import * as logger from 'tracer';
import services from '../../../../services';
import { massageItemResponse, massageListResponse } from '../../helpers/responseHelper';
const _console = logger.colorConsole();

export default function (fastify, opts, done) {
    fastify.post('/list', async (request, reply) => {
        try {
            const filters = {
                ...request.body,
                user: request.user,
            };
            const response = await services.tasks.controllers.taskBlocksCtrl.getAll(filters);
            const count = await services.tasks.controllers.taskBlocksCtrl.count(filters);
            return massageListResponse(
                response,
                {
                    totalRecords: count,
                },
                request.body
            );
        } catch (e) {
            reply.status(400).send({ message: e.message });
        }
    });
    fastify.get('/:id', async (request, reply) => {
        try {
            const user = request.user;
            const response = await services.tasks.controllers.taskBlocksCtrl.getById(
                request.params.id,
                user
            );
            return massageItemResponse(response, request.params);
        } catch (e) {
            reply.status(400).send({ message: e.message });
        }
    });
    fastify.post('/create', async (request, reply) => {
        try {
            const data = await services.tasks.controllers.taskBlocksCtrl.create({
                ...request.body,
                user: request.user,
            });
            return massageItemResponse(data);
        } catch (e) {
            reply.status(400).send({ message: e.message });
        }
    });
    fastify.put('/:id', async (request, reply) => {
        try {
            const data = await services.tasks.controllers.taskBlocksCtrl.update(
                {
                    ...request.body,
                },
                request.user
            );
            return massageItemResponse(data);
        } catch (e) {
            reply.status(400).send({ message: e.message });
        }
    });
    fastify.delete('/:id', async (request, reply) => {
        try {
            const user = request.user;
            const data = await services.tasks.controllers.taskBlocksCtrl.del(
                {
                    id: request.params.id,
                    slotId: request.body.slotId,
                },
                user
            );
            return massageItemResponse(data);
        } catch (e) {
            reply.status(400).send({ message: e.message });
        }
    });
    done();
}
