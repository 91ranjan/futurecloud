import * as logger from 'tracer';
import services from '../../../../services';
const _console = logger.colorConsole();

export default function (fastify, opts, done) {
    fastify.register(require('./activity.route'), { prefix: '/activity' });
    done();
}
