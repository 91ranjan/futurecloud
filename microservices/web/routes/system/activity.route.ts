import * as logger from 'tracer';

export default function (fastify, opts, done) {
    // fastify.post('/preferences', async (request, reply)
    fastify.register(require('./logs.route'), { prefix: '/logs' });
    done();
}
