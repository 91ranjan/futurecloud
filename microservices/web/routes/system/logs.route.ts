import * as logger from 'tracer';
import services from '../../../../services';
import { massageListResponse } from '../../helpers/responseHelper';
const _console = logger.colorConsole();

export default function (fastify, opts, done) {
    fastify.post('/list', async (request, reply) => {
        try {
            const response = await services.database.activity.logs.getAll(request.body);
            const count = await services.database.activity.logs.getCount(request.body);
            return massageListResponse(
                response,
                {
                    totalRecords: count,
                },
                request.body
            );
        } catch (e) {
            reply.status(400).send({ message: e.message });
        }
    });
    done();
}
