import { verifyUser } from '../helpers/session';

export default async function (fastify, opts, done) {
    // await fastify.register(require('middie'));
    // fastify.use(verifyUser);
    fastify.addHook('preValidation', verifyUser);
    fastify.register(require('./calendar/calendar.route'), { prefix: '/calendar' });
    fastify.register(require('./tasks/index.route'), { prefix: '/tasks' });
    fastify.register(require('./files/index.route'), { prefix: '/files' });
    fastify.register(require('./music/index.route'), { prefix: '/music' });
    fastify.register(require('./video/index.route'), { prefix: '/video' });
    fastify.register(require('./calls/index.route'), { prefix: '/calls' });
    fastify.register(require('./finance/index.route'), { prefix: '/finance' });
    fastify.register(require('./trips/index.route'), { prefix: '/trips' });
    fastify.register(require('./journals/index.route'), { prefix: '/journals' });
    fastify.register(require('./dashboard/index.route'), { prefix: '/dashboards' });
    fastify.register(require('./contacts/index.route'), { prefix: '/contacts' });
    fastify.register(require('./users/index.route'), { prefix: '/users' });
    // fastify.register(require('./files/proxy.route'), { prefix: '/proxy_file' });
    done();
}
