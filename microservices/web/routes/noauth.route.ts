export default async function (fastify, opts, done) {
    fastify.register(require('./users/users_auth.route'), { prefix: '/users/auth' });
    fastify.register(require('./files/proxy.route'), { prefix: '/proxy_file' });
    done();
}
