import * as logger from 'tracer';
import services from '../../../../services';
import { massageItemResponse, massageListResponse } from '../../helpers/responseHelper';
const _console = logger.colorConsole();

export default function (fastify, opts, done) {
    fastify.post('/list', async (request, reply) => {
        try {
            const filters = request.body;
            const data = await services.calendar.controllers.events.getAll(filters);
            const totalRecords = await services.calendar.controllers.events.count(filters);
            return massageListResponse(
                data,
                {
                    totalRecords,
                },
                filters
            );
        } catch (e) {
            throw e;
        }
    });
    fastify.post('/create', async (request, reply) => {
        try {
            const user = request.user;
            const event = await services.calendar.controllers.events.create({
                ...request.body,
                organizer: user.id,
            });
            return massageItemResponse(event);
        } catch (e) {
            reply.status(400).send({ message: e.message });
        }
    });
    fastify.put('/:id', async (request, reply) => {
        try {
            const user = request.user;
            const event = await services.calendar.controllers.events.update({
                ...request.body,
                organizer: user.id,
            });
            return massageItemResponse(event);
        } catch (e) {
            reply.status(400).send({ message: e.message });
        }
    });
    fastify.delete('/:id', async (request, reply) => {
        try {
            const user = request.user;
            const event = await services.calendar.controllers.events.del({
                id: request.params.id,
                organizer: user.id,
            });
            return massageItemResponse(event);
        } catch (e) {
            reply.status(400).send({ message: e.message });
        }
    });

    done();
}
