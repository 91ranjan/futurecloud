import * as logger from 'tracer';
import services from '../../../../services';
import { massageItemResponse, massageListResponse } from '../../helpers/responseHelper';
const _console = logger.colorConsole();

export default function (fastify, opts, done) {
    fastify.post('/list', async (request, reply) => {
        try {
            const filters = request.body;
            const data = await services.video.controllers.playlist.getAll(filters);
            const totalRecords = await services.video.controllers.playlist.count(filters);
            return massageListResponse(
                data,
                {
                    totalRecords,
                },
                filters
            );
        } catch (e) {
            throw e;
        }
    });
    fastify.get('/:id', async (request, reply) => {
        try {
            const data = await services.video.controllers.playlist.getById(request.params.id);
            return massageListResponse(data, {}, request.params);
        } catch (e) {
            throw e;
        }
    });
    fastify.post('/create', async (request, reply) => {
        try {
            const playlist = await services.video.controllers.playlist.create({
                ...request.body,
                user: request.user,
            });
            return massageItemResponse(playlist);
        } catch (e) {
            reply.status(400).send({ message: e.message });
        }
    });
    fastify.delete('/:id', async (request, reply) => {
        try {
            const playlist = await services.video.controllers.playlist.del({
                id: request.params.id,
                user: request.user,
            });
            return massageItemResponse(playlist);
        } catch (e) {
            reply.status(400).send({ message: e.message });
        }
    });
    fastify.put('/:id', async (request, reply) => {
        try {
            const playlist = await services.video.controllers.playlist.update({
                ...request.body,
                user: request.user,
            });
            return massageItemResponse(playlist);
        } catch (e) {
            reply.status(400).send({ message: e.message });
        }
    });
    done();
}
