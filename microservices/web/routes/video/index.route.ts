import * as logger from 'tracer';
import services from '../../../../services';
import { massageItemResponse, massageListResponse } from '../../helpers/responseHelper';
const _console = logger.colorConsole();

export default function (fastify, opts, done) {
    fastify.post('/list', async (request, reply) => {
        try {
            const data = await services.video.controllers.list.getAll(request.body);
            const count = await services.video.controllers.list.count(request.body);

            return massageListResponse(
                data,
                {
                    totalRecords: count,
                },
                request.body
            );
        } catch (e) {
            reply.status(400).send({ message: e.message });
        }
    });
    fastify.put('/:id', async (request, reply) => {
        try {
            const data = await services.video.controllers.list.update(request.body);
            return massageItemResponse(data, {});
        } catch (e) {
            reply.status(400).send({ message: e.message });
        }
    });

    fastify.register(require('./playlist.route'), { prefix: '/playlist' });

    done();
}
