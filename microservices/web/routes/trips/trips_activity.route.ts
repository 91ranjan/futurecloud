import * as logger from 'tracer';
import services from '../../../../services';
import { massageItemResponse, massageListResponse } from '../../helpers/responseHelper';
const _console = logger.colorConsole();

export default function (fastify, opts, done) {
    fastify.post('/list', async (request, reply) => {
        try {
            const filters = {
                ...request.body,
                user: request.user,
            };
            const response = await services.trips.controllers.activity.getAll(filters);
            const count = await services.trips.controllers.activity.count(filters);
            return massageListResponse(
                response,
                {
                    totalRecords: count,
                },
                request.body
            );
        } catch (e) {
            reply.status(400).send({ message: e.message });
        }
    });
    fastify.get('/:id', async (request, reply) => {
        try {
            const user = request.user;
            const response = await services.trips.controllers.activity.getById(request.params.id, user);
            return massageItemResponse(response, request.params);
        } catch (e) {
            reply.status(400).send({ message: e.message });
        }
    });
    fastify.post('/create', async (request, reply) => {
        try {
            const data = await services.trips.controllers.activity.create({
                ...request.body,
                user: request.user,
            });
            return massageItemResponse(data);
        } catch (e) {
            reply.status(400).send({ message: e.message });
        }
    });
    fastify.put('/:id', async (request, reply) => {
        try {
            const data = await services.trips.controllers.activity.update(
                {
                    ...request.body,
                },
                request.user
            );
            return massageItemResponse(data);
        } catch (e) {
            reply.status(400).send({ message: e.message });
        }
    });
    fastify.delete('/:id', async (request, reply) => {
        try {
            const data = await services.trips.controllers.activity.del(
                {
                    id: request.params.id,
                },
                request.user
            );
            return massageItemResponse(data);
        } catch (e) {
            reply.status(400).send({ message: e.message });
        }
    });
    done();
}
