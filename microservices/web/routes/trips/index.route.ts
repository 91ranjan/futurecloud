import * as logger from 'tracer';

export default function (fastify, opts, done) {
    // fastify.post('/preferences', async (request, reply)
    fastify.register(require('./trips.route'), { prefix: '/trips' });
    fastify.register(require('./trips_checklist.route'), { prefix: '/checklist' });
    fastify.register(require('./trips_places.route'), { prefix: '/places' });
    fastify.register(require('./trips_commute.route'), { prefix: '/commute' });
    fastify.register(require('./trips_expense.route'), { prefix: '/expense' });
    fastify.register(require('./trips_lodge.route'), { prefix: '/lodge' });
    done();
}
