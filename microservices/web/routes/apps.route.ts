const cmd = require('node-cmd');
import * as logger from 'tracer';
import { LogLevel, LogType } from '../../activity/constant/messageConstants';
import { logActivity } from '../../activity/logger';
import GlobalVars from '../../../config/dev';

const _console = logger.colorConsole();

async function logError(request, reply, error) {
    if (
        reply.raw.statusCode === 400 ||
        reply.raw.statusCode === 404 ||
        reply.raw.statusCode === 500
    ) {
        logActivity({
            app: 'web',
            level: LogLevel.Error,
            type: LogType.Modules,
            title: 'Error',
            messages: {
                customMessage: error,
                exception: reply.raw.statusCode === 400,
            },
            method: request.raw.method,
            url: request.raw.url,
        });
    }
    // _console.log(Object.keys(request.raw));
    // _console.log(request.raw.client);
}

export default async function (fastify, opts, done) {
    await fastify.register(require('middie'));
    await fastify.register(require('fastify-cookie'), {
        secret: GlobalVars.session_secret, // for cookies signature
        hook: false, // set to false to disable cookie autoparsing or set autoparsing on any of the following hooks: 'onRequest', 'preParsing', 'preHandler', 'preValidation'. default: 'onRequest'
        parseOptions: {}, // options for parsing cookies
    });
    fastify.addHook('onSend', logError);
    fastify.addHook('onError', logError);
    fastify.register(require('./noauth.route'), { prefix: '/' });
    fastify.register(require('./withauth.route'), { prefix: '/' });
    fastify.post('/update-web', async (request, reply) => {
        try {
            cmd.run(
                `
            cd ../../../futurecloud-web/
            git pull
            yarn
            yarn build
            sudo cp -R build/public build/images /var/www/html/futurecloud/
            `,
                (err, data, stderr) => {
                    if (err) {
                        throw err;
                    } else {
                        reply.send(data);
                    }
                }
            );
        } catch (e) {
            reply.status(400).send({ message: e.message });
        }
    });
    fastify.post('/update-backend', async (request, reply) => {
        try {
            console.log('==========Updating backend code ===========');
            cmd.run(
                `
            cd ../../
            git pull
            `,
                (err, data, stderr) => {
                    if (err) {
                        throw err;
                    } else {
                        reply.send(data);
                    }
                }
            );
        } catch (e) {
            reply.status(400).send({ message: e.message });
        }
    });
    done();
}
