import * as logger from 'tracer';

export default function (fastify, opts, done) {
    // fastify.post('/preferences', async (request, reply)
    fastify.register(require('./dashboards.route'), { prefix: '/' });
    done();
}
