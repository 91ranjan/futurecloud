import * as logger from 'tracer';
import services from '../../../../services';
import * as jwt from 'jsonwebtoken';
import * as superagent from 'superagent';
import * as got from 'got';
import cloudFileManager from '../../../../services/files/utils/cloudFileManager';
const _console = logger.colorConsole();
import GlobalVars from '../../../../config/dev';

export default function (fastify, opts, done) {
    fastify.get('/', async (request, reply) => {
        try {
            const token = request.query.token;
            if (!token) {
                reply.status(400).send({ message: "'token' key is missing." });
                return;
            }
            const user = jwt.verify(request.query.token, GlobalVars.session_secret);
            let response = await services.files.getDownloadLink(request.query.fileid, user);

            reply
                .header('Content-Length', 9531925 * 4)
                .header('Content-Range', "bytes")
                .header('Accept-Ranges', "bytes")
                .send(got.stream(response.downloadLink));

            // Download the file and send it back.
            // superagent
            //     .get(response.downloadLink)
            //     .buffer(true)
            //     // .parse(superagent.parse['application/octet-stream'])
            //     .then((resp) => reply.send(resp.body));
        } catch (e) {
            console.error(e);
            reply.status(400).send({ message: e.message });
        }
    });
    done();
}
