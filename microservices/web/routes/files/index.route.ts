import * as logger from 'tracer';
import services from '../../../../services';
import { massageItemResponse, massageListResponse } from '../../helpers/responseHelper';
import serverConfig from '../../../../config/dev';
import * as fs from 'fs-extra';
const _console = logger.colorConsole();
const pathFunc = require('path');

export default function (fastify, opts, done) {
    fastify.post('/upload', async (request, reply) => {
        try {
            if (!request.headers['uploader-parent-directory']) {
                reply.status(400).send({
                    message: "Failed to upload parent directory not passed."
                })
                return;
            }
            const isLastChunk =
                request.headers['uploader-chunk-number'] ===
                request.headers['uploader-chunks-total'];
            let response = <any>await services.files.upload(request);
            if (isLastChunk) {
                // Verify if all the chunks were written properly.
                // Creating the file entry.
                response = await services.database.files.files.create({
                    name: request.headers['uploader-file-id'],
                    path: response.path,
                    dir: response.dir,
                    metadata: response.metadata,
                    thirdPartyData: response.thirdPartyData,
                    isLocal: response.isLocal,
                    link: response.link,
                    download_link: response.download_link,
                    kind: 'FILE',
                    favorite: false,
                    size: response.size,
                    parent: request.headers['uploader-parent-directory'],
                    user: request.user,
                    storage: response.storage._id,
                });
                // Getting all the sub schema populated.
                response = await services.database.files.files.getById(response.id);
                // To be read in music controller
                services.events.broadcastEvent(services.files.constants.event_types.FILE_UPLOAD_SUCCESS, response);

                // Adding the files to specific libraries
                
                if (services.video.constants.supportedext.includes(response.metadata.ext) 
                && services.video.constants.supportedMime.includes(response.metadata.mime)) {
                    // 2. Checking for video file
                    _console.debug('Adding the file to the video.')
                    const file = response.toJSON();
                    file.user = request.user;
                    await services.video.controllers.list.addVideoFromFile(file)
                }
            } else {
                response = {
                    ...request.headers,
                };
            }
            return massageItemResponse(response, {});
        } catch (e) {
            reply.status(400).send({ message: e.message });
        }
    });
    fastify.post('/create', async (request, reply) => {
        try {
            const { name, path } = request.body;
            let response = await services.files.createFolder(name, path, request.user);
            const reqFolder = request.body;
            reqFolder.path = request.user.local.username + reqFolder.dir;
            const data = await services.database.files.files.create({
                ...reqFolder,
                isLocal: response.isLocal,
                thirdPartyData: response.thirdPartyData,
                user: request.user.id,
            });
            return massageItemResponse(data, {});
        } catch (e) {
            reply.status(400).send({ message: e.message });
        }
    });
    fastify.post('/uploadFromUrl', async (request, reply) => {
        try {
            const {dir, fileId} = request.body;
            let file = await services.files.uploadFromUrl(request.body, request.user);
                // Verify if all the chunks were written properly.
                // Creating the file entry.
            const response = await services.database.files.files.create({
                name: file.name,
                path: file.path,
                isLocal: file.isLocal,
                dir: dir,
                metadata: file.metadata,
                kind: 'FILE',
                favorite: false,
                size: 0,
                parent: fileId,
                user: request.user,
            });
            if ( services.music.constants.supportedext.includes(response.metadata.ext) 
                && services.music.constants.supportedMime.includes(response.metadata.mime)) {
                    _console.debug('Adding the file to the music.')
                const file = response.toJSON();
                file.user = request.user;
                await services.music.controllers.list.addMusicFromFile(file)
            }
            return massageItemResponse(response, {});
        } catch (e) {
            reply.status(400).send({ message: e.message });
        }
    });
    fastify.post('/list', async (request, reply) => {
        try {
            const filters = request.body;
            if (request.body.parent && request.body.parent.hasOwnProperty('exists')) {
                // It is looking for root chidrens
                let rootFolder = await services.files.getRootFolder(request.user);
                filters.parent = rootFolder.id;
            }

            const data = await services.files.getAll(filters);
            const count = await services.files.count(filters);
            return massageListResponse(
                data,
                {
                    totalRecords: count,
                },
                request.body
            );
        } catch (e) {
            reply.status(400).send({ message: e.message });
        }
    });
    fastify.get('/:id/download_link', async (request, reply) => {
        try {
            let data = await services.files.getDownloadLink( request.params.id,  request.user);
            return massageItemResponse(data, {});
        } catch (e) {
            console.error(e);
            reply.status(400).send({ message: e.message });
        }
    });
    fastify.post('/getFolderByPath', async (request, reply) => {
        try {
            const path = request.body.path;
            const data = await services.files.getFolderByPath(path, request.user);
            return massageListResponse(data, {}, request.body);
        } catch (e) {
            reply.status(400).send({ message: e.message });
        }
    });
    fastify.post('/getParentFolders', async (request, reply) => {
        try {
            const data = await services.files.getParentFolders(request.body.path, request.user);
            return massageListResponse(data, {}, request.body);
        } catch (e) {
            reply.status(400).send({ message: e.message });
        }
    });
    fastify.delete('/:id', async (request, reply) => {
        try {
            const fileId = request.params.id;
            const data = await services.files.del(fileId, request.user);
            services.events.broadcastEvent(services.files.constants.event_types.FILE_DELETE_SUCCESS, data);
            return massageItemResponse(data, {});
        } catch (e) {
            reply.status(400).send({ message: e.message });
        }
    });
    done();
}
