import * as logger from 'tracer';
import services from '../../../../services';
import { massageItemResponse, massageListResponse } from '../../helpers/responseHelper';
const _console = logger.colorConsole();

export default function (fastify, opts, done) {
    fastify.post('/list', async (request, reply) => {
        try {
            const data = await services.music.controllers.list.getAll(request.body);
            const count = await services.music.controllers.list.count(request.body);

            return massageListResponse(
                data,
                {
                    totalRecords: count,
                },
                request.body
            );
        } catch (e) {
            reply.status(400).send({ message: e.message });
        }
    });
    fastify.put('/:id', async (request, reply) => {
        try {
            const data = await services.music.controllers.list.update(request.body);
            return massageItemResponse(data, {});
        } catch (e) {
            reply.status(400).send({ message: e.message });
        }
    });
    fastify.get('/:music_id/download_request_link', async (request, reply) => {
        try {
            let data = await services.music.controllers.list.getDownloadRequestLink(
                request.params.music_id,
                request.user
            );
            services.events.broadcastEvent(
                services.music.constants.events.MUSIC_PLAY_SUCCESS,
                request.params.music_id
            );
            return massageItemResponse(data, {});
        } catch (e) {
            console.error(e);
            reply.status(400).send({ message: e.message });
        }
    });
    fastify.get('/:music_id/download_link', async (request, reply) => {
        try {
            let data = await services.music.controllers.list.getDownloadLink(
                request.params.music_id,
                request.user
            );
            services.events.broadcastEvent(
                services.music.constants.events.MUSIC_PLAY_SUCCESS,
                request.params.music_id
            );
            return massageItemResponse(data, {});
        } catch (e) {
            console.error(e);
            reply.status(400).send({ message: e.message });
        }
    });

    fastify.register(require('./playlist.route'), { prefix: '/playlist' });

    done();
}
