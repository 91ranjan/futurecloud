import * as logger from 'tracer';
import { verifyUser } from '../../helpers/session';

export default function (fastify, opts, done) {
    // fastify.post('/preferences', async (request, reply)
    fastify.addHook('preValidation', verifyUser);
    fastify.register(require('./storage.route'), { prefix: '/storage' });
    fastify.register(require('./module.route'), { prefix: '/modules' });
    done();
}
