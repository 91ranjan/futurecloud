import * as logger from 'tracer';

export default function (fastify, opts, done) {
    // fastify.post('/preferences', async (request, reply)
    fastify.register(require('./contacts.route'), { prefix: '/' });
    fastify.register(require('./contact_books.route'), { prefix: '/books' });
    done();
}
