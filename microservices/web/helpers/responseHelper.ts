export function massageItemResponse(data, metadata = {}) {
    return { data, metadata };
}

export function massageListResponse(data, metadata = {}, filters) {
    return { data, metadata, filters };
}
