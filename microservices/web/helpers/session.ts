import GlobalVars from '../../../config/dev';
import * as jwt from 'jsonwebtoken';
var cookies = require('cookies');
import * as logger from 'tracer';
const _console = logger.colorConsole();

export const verifyUser = (req, res, next) => {
    var token = req.headers.authorization;
    if (!token) {
        token = new cookies(req, res).get('access_token');
    }
    jwt.verify(token, GlobalVars.session_secret, function (err, user) {
        if (err) {
            _console.error(err);
            res.status(400);
            res.json({
                status: 'FAILURE',
                message: 'User authentication failed',
            });
        } else {
            req.user = user;
            next();
        }
    });
};
