import { keyMirror } from "../../common/utils/keymirror";
import config from "../config";

export default {
    name: 'Web',
    app_key: 'web',
    baseTopic: 'web',
    port: config.web.port,
};
