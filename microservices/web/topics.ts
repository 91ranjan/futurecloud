import { createMicroServiceTopics } from '../../common/createTopics';
import Config from './config';

const serviceTopics = createMicroServiceTopics(Config.baseTopic);

export default {
    topics: [
        // Common topics
        ...serviceTopics,
    ],
};
