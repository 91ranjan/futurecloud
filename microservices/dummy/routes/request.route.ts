import { massagePayload } from '../../../common/helpers/httprequest.helpers';
import { del, get, post, put } from '../controllers/request.ctrl';

export default function (fastify, opts, done) {
    fastify.get('/request', async (request, reply) => {
        try {
            const response = await get(request);
            return massagePayload(response, request, null);
        } catch (e) {
            return massagePayload(e, request, e.message);
        }
    });

    fastify.post('/request', async (request, reply) => {
        try {
            const response = await post(request);
            return massagePayload(response, request, null);
        } catch (e) {
            return massagePayload(e, request, e.message);
        }
    });

    fastify.put('/request', async (request, reply) => {
        try {
            const response = await put(request);
            return massagePayload(response, request, null);
        } catch (e) {
            return massagePayload(e, request, e.message);
        }
    });

    fastify.delete('/request', async (request, reply) => {
        try {
            const response = await del(request);
            return massagePayload(response, request, null);
        } catch (e) {
            return massagePayload(e, request, e.message);
        }
    });
    done();
}
