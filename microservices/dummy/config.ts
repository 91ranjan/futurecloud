import { keyMirror } from "../../common/utils/keymirror";
import config from "../config";

export default {
    name: 'Dummy Microservice',
    app_key: 'dummy',
    baseTopic: 'dummy',
    port: config.dummy.port,
    tables: keyMirror("log"),
};
