import * as mongoose from 'mongoose';
import * as bluebird from 'bluebird';
import serverconfig from '../../config/dev';
import * as logger from 'tracer';
import fastify from 'fastify';

import config from './config';
import topics from './topics';
import { del, get, post, put } from './controllers/request.ctrl';
import { massagePayload } from '../../common/helpers/httprequest.helpers';

const _console = logger.colorConsole();
const server = fastify({ logger: true });

server.register(require('./routes/request.route'), { prefix: '/request' });

server.listen(config.port, function (err, address) {
    if (err) {
        server.log.error(err);
        process.exit(1);
    }
    server.log.info(`${config.name} server listening on ${address}`);
});
