import { RequestType } from '../../common/constants/request.constant';
import config from './config';
import { del, get, post, put } from '../../common/utils/JsonApi';
import { getModel } from '../database/controllers/request.ctrl';

const createRequestPayload = (type, data, table, app_key, method) => {

    const payload = {
        type,
        table,
        app_key,
        filters: null,
        payload: null,
        method,
    };
    if(type === (<any>RequestType).GET) {
        payload.filters =   data;
    } else {
        payload.payload = data
    }
    try {
        // Request payload validated
        return getModel(payload) ? payload : null;
    } catch (e) {
        throw e;
    }
};

export default {
    http: {
        request: {
            create: {
                call: (payload, filters = {}) => {
                    return post(`http://localhost:${config.port}/request`, payload, filters);
                },
                url: '/request',
                createPayload: createRequestPayload,
            },
            get: {
                call: (payload, filters = {}) => {
                    return post(`http://localhost:${config.port}/request`, payload, filters);
                },
                url: '/request',
                createPayload: createRequestPayload,
            },
            update: {
                call: (payload, filters = {}) => {
                    return put(`http://localhost:${config.port}/request`, payload, filters);
                },
                url: '/request',
                createPayload: createRequestPayload,
            },
            delete: {
                call: (payload, filters = {}) => {
                    return del(`http://localhost:${config.port}/request`, payload, filters);
                },
                url: '/request',
                createPayload: createRequestPayload,
            },
        },
    },
};
