import * as logger from 'tracer';
import { RequestType } from '../../../common/constants/request.constant';
import { errorParse, responseParser } from '../../../common/helpers/httprequest.helpers';
import databaseInterface from '../../database/interface';
import config from '../config';
const _console = logger.colorConsole();

export async function get(request) {
    try {
        // Prepare the request to the db service
        let endpoint, payload, data;
        endpoint = databaseInterface.http.request.create;
        const username = request.body.payload.username;
        const email = request.body.payload.email;
        const password = request.body.payload.password;
        const dbFilters = {
            username, email, password
        }

        payload = endpoint.createPayload((<any>RequestType).GET, dbFilters, (<any>config.tables).list, config.app_key, "validateUser")
        data = await databaseInterface.http.request.get.call(payload);
        
        // Handle the data response from db
        const response = responseParser(data);
        if(response.error) {
            throw new Error(response.error)
        } else {
            return response.payload
        }
    } catch(e) {
        if(e.response) {
            // Http error
            throw new Error(errorParse(e));
        } else {
            throw e;
        }
    }
}

export async function post(request) {
   
}

export async function put(request) {
    
}

export async function del(request) {

}
