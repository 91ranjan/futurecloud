export default {
    web: {
        port: '3001',
    },
    activity: {
        port: '3003',
    },
    dummy: {
        port: '3000',
    },
};
