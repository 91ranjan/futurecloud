import * as pm2 from 'pm2';
import Service from './config';
import * as logger from 'tracer';
import topics from './topics';

const _console = logger.colorConsole();

function startService(service) {
    pm2.start(
        {
            name: service.name,
            script: `ts-node --project tsconfig.json -- ${__dirname + "/" + service.path}`,
            max_memory_restart: '1000M',
            // watch: true,
        },
        (err, apps) => {
            if (err) {
                console.error(err);
            } else {
                console.log(`${service.name} service started.`);
            }
        }
    );
}

// 1. Initializing the Database service
pm2.connect(() => {
    pm2.list(function (err, processes) {
        if (err) {
            throw err;
        }
        pm2.launchBus((err, bus) => {
            // Starting other services
            Service.services.forEach((_service) => {
                const isServiceRunning = processes.some(
                    (_process) =>
                        _process.name === _service.name && _process.pm2_env.status === 'online'
                );
                if (!isServiceRunning) {
                    startService(_service);
                } else {
                    console.log(`${_service.name} service already running.`);
                }
            });
            // Starting module manager
            bus.on('error', console.error);
        });
    });
});
