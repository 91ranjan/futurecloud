export default {
    services: [
        {
            name: 'Activity',
            path: '../activity/index.ts',
            topic: 'activity',
        },
        {
            name: 'Web',
            path: '../web/index.ts',
            topic: 'web',
        },
    ],
    cluster_count: 1,
    name: 'Service Manager',
    baseTopic: 'servicemgr',
};
