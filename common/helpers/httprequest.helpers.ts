export function massagePayload(payload, request, error) {
    return {
        status: error ? "FAILURE" : "SUCCESS",
        payload,
        req_payload: request.body,
        req_query: request.query,
        error: error
    }
}

export function responseParser(resp) {
    if (resp.statusCode === 200 || resp.statusCode === 201) {
        return JSON.parse(resp.text);
    }
    return resp;
}

export function errorParse(err) {
    return err.response.body.error || JSON.parse(err.response.text);
}