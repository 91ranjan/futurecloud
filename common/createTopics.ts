import { LifeCycleActionType } from './constants/microservices';
const defaultPrams = {
    partitions: 1,
    replicationFactor: 1,
};

const genericActionTypes = [
    LifeCycleActionType.INIT,
    LifeCycleActionType.STOP,
    LifeCycleActionType.RESTART,
    LifeCycleActionType.HEALTH,
    LifeCycleActionType.ERROR,
    LifeCycleActionType.MESSAGE,
];

export function createTopicName(baseTopic, action) {
    return baseTopic + '_' + action;
}

export function createMicroServiceTopics(
    baseTopics,
    actions = LifeCycleActionType,
    params = defaultPrams
) {
    return genericActionTypes.map((_type) => {
        return {
            topic: `${baseTopics}_${LifeCycleActionType[_type]}`,
            ...params,
        };
    });
}
