import * as superagent from 'superagent';
import * as qs from 'query-string';

export function get(url, data, opts?) {
    return superagent
        .get(url)
        .query(qs.stringify(data))
        .set('Content-Type', 'application/json')
        .set('Accept', 'application/json');
}

export function post(url, data, filters = {}, opts?) {
    return superagent
        .post(url)
        .query(qs.stringify(filters))
        .set('Content-Type', 'application/json')
        .set('Accept', 'application/json')
        .send(data);
}

export function put(url, data, filters = {}, opts?) {
    return superagent
        .put(url)
        .query(qs.stringify(filters))
        .set('Content-Type', 'application/json')
        .set('Accept', 'application/json')
        .send(data);
}

export function del(url, data, filters = {}, opts?) {
    return superagent
        .delete(url)
        .query(qs.stringify(filters))
        .set('Content-Type', 'application/json')
        .set('Accept', 'application/json')
        .send(data);
}
