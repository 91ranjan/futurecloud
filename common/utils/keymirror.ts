export function keyMirror(...args) {
    const ret = {};
    for (let key in args) {
        const val = args[key]
        ret[val] = val;
    }
    return ret;
}
