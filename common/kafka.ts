import * as kafka from 'kafka-node';
import * as logger from 'tracer';
import { MessageType } from './constants/message';
const _console = logger.colorConsole();

export function setupConnection() {
    const client = new kafka.KafkaClient({ kafkaHost: 'localhost:9092' });
    const Consumer = kafka.Consumer;
    const Producer = kafka.Producer;
    return { client, Consumer, Producer };
}

export function createConsumer({ client, Consumer }, topics) {
    const consumer = new Consumer(client, topics, {
        autoCommit: true,
    });
    consumer.on('error', function (err) {
        console.error(err);
        if (err.topics && err.topics.length && err.message.endsWith('do not exist')) {
            client.createTopics(topics, (error, result) => {
                if (error) {
                    console.error(error);
                } else {
                    if (result.length) {
                        console.error(result);
                    } else {
                        console.log(`Created topics for database.`);
                        createConsumer({ client, Consumer }, topics);
                    }
                }
            });
        }
    });
    return consumer;
}

export function createProducer({Producer, client},) {
    return new Producer(client);
}

const a = {
    // Request
    id: 'Sender assigned an id to trace it back.',
    reqData: 'Sender sent a data json ',
    reqTime: 'Time it was sent on',
    reqService: 'Service Name',

    // Response
    respData: 'Response Data',
    respTime: 'Time',
    respService: 'Service Name',
    type: 'Message Type'
}

/**
 * 
 * @param producer Producer of the kafka.
 * @param service The service which is sending message.
 * @param payloads Payload sent by the service.
 */
export function sendMessage(producer, to, service, payloads = [], request?) {
    let commonPayload = {id: null, reqData: null, reqTime: null, reqService: null}
    if(request && typeof request.value == "object") {
        const {id, reqData, reqTime, reqService} = JSON.parse(request.value);
        commonPayload = {id, reqData,reqTime,reqService};
    }

    (<any>payloads) = (<any>payloads).map(_data => {
        if(typeof(_data) === 'object') {
            _data = JSON.stringify(_data);
        }
        if(typeof _data !== 'string') {
            _console.error('Message is not a string....')
        }
        
        const _payload = <any>{...commonPayload};
        _payload.topic = to
        _payload.timestamp= Date.now()
        _payload.type = request && request.value ? MessageType.RESPONSE : MessageType.REQUEST;
        _payload.respData = _data;
        _payload.respTime = Date.now();
        _payload.respService = service.baseTopic
        return _payload
    });
    producer.send(payloads, function (err, data) {
        if(err) {
            console.error(err);
        } else {
            // Payload is sent
        }
    });
}
