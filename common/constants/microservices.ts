export const LifeCycleActionType = {
    INIT: 'INIT',
    STOP: 'STOP',
    RESTART: 'RESTART',
    HEALTH: 'HEALTH',
    ERROR: 'ERROR',
    MESSAGE: 'MESSAGE',
};
