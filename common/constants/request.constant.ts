import { keyMirror } from '../utils/keymirror';

export const RequestType = keyMirror('INIT', 'CREATE', 'UPDATE', 'DELETE', 'GET', 'UPLOAD');