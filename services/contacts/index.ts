import contactsCtrls from './controllers/contacts.ctrl';
import contactBooksCtrls from './controllers/contact_books.ctrl';

export default {
    controllers: {
        list: contactsCtrls,
        books: contactBooksCtrls,
    },
};
