import * as logger from 'tracer';
const _console = logger.colorConsole();

import services from '../../index';

export async function getAll(filters) {
    try {
        return await services.database.contacts.list.getAll(filters);
    } catch (e) {
        throw e;
    }
}

export async function count(filters) {
    try {
        return await services.database.contacts.list.getCount(filters);
    } catch (e) {
        throw e;
    }
}

export async function getById(id, user) {
    try {
        return await services.database.contacts.list.getById(id, user);
    } catch (e) {
        throw e;
    }
}

export async function create(event) {
    try {
        return await services.database.contacts.list.create(event);
    } catch (e) {
        throw e;
    }
}

export async function update(event, user) {
    try {
        return await services.database.contacts.list.update(event, user);
    } catch (e) {}
}

export async function del(request, user) {
    const { id } = request;
    try {
        return await services.database.contacts.list.delete({ id }, user);
    } catch (e) {
        throw e;
    }
}

export default {
    getAll,
    count,
    getById,
    create,
    update,
    del,
};
