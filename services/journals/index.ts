import journalBooksCtrl from './controllers/journal_books.ctrl';
import journalEntriesCtrl from './controllers/journal_entries.ctrl';

export default {
    controllers: { books: journalBooksCtrl, entries: journalEntriesCtrl },
};
