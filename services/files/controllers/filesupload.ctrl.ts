import uploader from '../utils/uploader';
import * as logger from 'tracer';
import * as mongoose from 'mongoose';
import * as fs from 'fs-extra';
const fsFile = require('fs');
const pathFunc = require('path');
const ytdl = require('ytdl-core');
import serverConfig from '../../../config/dev';
import services from '../../index';
import { asyncForEach } from '../../../common/utils/asyncHelpers';
const FileType = require('file-type');
const ffmpegPath = require('@ffmpeg-installer/ffmpeg').path;
let ffmpeg = require('fluent-ffmpeg');
import {captureMusicMetadata} from '../helpers/metadata';
import cloudFileManager from '../utils/cloudFileManager';

ffmpeg.setFfmpegPath(ffmpegPath);

const _console = logger.colorConsole();
const maxFileSize = 100;
const maxChunkSize = 500;


function getDirPath (username, path){
    return pathFunc.join(serverConfig.publicpath , '/' + username + '/' + path);
}

export async function getAll(filters) {
    try {
        return await services.database.files.files.getAll(filters);
    } catch (e) {
        throw e;
    }
}

export async function getRootFolder(user) { 
    let rootFolder = await services.files.getByRootFolder({ user: user.id, kind: "FOLDER", name: user.local.username })
    const storage = await services.settings.controllers.storage.getAll({ user: user.id, isActive: true });
    if (!rootFolder) {
        // Create one
        const dirPath = pathFunc.join(
            serverConfig.publicpath,
            '/' + user.local.username
        );
        if (!storage.length) {
            // create an actual directory only when there is no cloud storage service registed.
            await fs.mkdirpSync(dirPath);
        }
        rootFolder = await services.files.create({
            name: user.local.username,
            dir: '/',
            isLocal: !storage.length,
            path: user.local.username + '/',
            kind: 'FOLDER',
            user: user.id,
        });
    }
    return rootFolder;
}

async function recurseFindChildren(folders, parentId) {
    const folder = folders.shift();
    const dbFolder = await services.database.files.files.getByRootFolder({
        parent: parentId,
        name: folder,
        kind: "FOLDER"
    });
    if (dbFolder) {
        const parentFolders = [dbFolder];
        if (folders.length) {
            const children = await recurseFindChildren(folders, dbFolder.id);
            return parentFolders.concat(children);
        } else {
            return [dbFolder]
        }
    } else {
        return undefined;
    }
}

export async function getFolderByPath(path, user) {
    try {
        let rootFolder = await getRootFolder(user);
        const folders = path.split('/').filter(_dir=>_dir);
        let lastFolder;
        if(folders.length) {
            const parentFolders = await recurseFindChildren(folders, rootFolder.id)
            lastFolder = parentFolders.slice(-1);
        } else {
            lastFolder = rootFolder
        }

        return [lastFolder];
    } catch (e) {
        console.error(e);
        throw e;
    }
}

export async function getParentFolders(path, user) {
    try {
        const rootFolder = await getRootFolder(user);
        
        const folders = path.split('/').filter(_dir=>_dir);
        let parentFolders;
        if(folders.length) {
            parentFolders = await recurseFindChildren(folders, rootFolder.id)
            parentFolders.unshift(rootFolder)
        } else {
            parentFolders = [rootFolder]
        }

        return parentFolders;
    } catch (e) {
        throw e;
    }
}

export async function count(filters) {
    try {
        return await services.database.files.files.getCount(filters);
    } catch (e) {
        throw e;
    }
}

export async function getById(id) {
    try {
        return await services.database.files.files.getById(id);
    } catch (e) {
        throw e;
    }
}

export async function getByRootFolder(filters) {
    try {
        return await services.database.files.files.getByRootFolder(filters);
    } catch (e) {
        throw e;
    }
}

export async function create(file) {
    try {
        return await services.database.files.files.create(file);
    } catch (e) {
        throw e;
    }
}

export async function update(file) {
    try {
        return await services.database.files.files.update(file);
    } catch (e) {
        throw e;
    }
}

export async function getDownloadLink(id, user) {
    try {
        const storage = await services.settings.controllers.storage.getAll({ user: user.id, isActive: true });
        const file = await services.database.files.files.getById(id);
        const fileType = file.metadata.mime; 
        let downloadLink;
        if (storage.length) {
            if ( services.music.constants.supportedMime.includes(fileType)) {
                downloadLink = await cloudFileManager.downloadAudioLink(file.thirdPartyData, storage[0])
            } else {
                downloadLink = await cloudFileManager.downloadLink(file.thirdPartyData, storage[0])
            }
        } else {
            downloadLink =  '/public/' + file.path + file.name
        }
        return {
            file,
            downloadLink
        }
    } catch (e) {
        throw e;
    }
}

async function recurseDelChildren(fileId, userId) {
    const dbFile = await services.database.files.files.getByRootFolder({_id: fileId, user: userId});
    if (dbFile.kind === "FOLDER") {
        const children = await services.database.files.files.getAll({
            parent: fileId,
        });
        await asyncForEach(children, async _child => {
            await recurseDelChildren(_child.id, userId);
        })
        
    }
    return await services.database.files.files.delete({id: fileId, userId});
}


async function recurseFindParents(fileId, userId) {
    let parentFiles = <any[]>[]
    const file = await services.database.files.files.getByRootFolder({_id: fileId, user: userId});
    parentFiles.push(file);
    if (file.parent) {
        const parentArray = await recurseFindParents(file.parent.id, userId);
        return parentArray.concat(parentFiles);
    } else {
        return [file];
    }
}

export async function del(fileId, user) {
    try {
        // Remove the actual dir
        // converting it to mongoose object id
        fileId = mongoose.Types.ObjectId(fileId)
        const file = await services.database.files.files.getById(fileId);
        if (file.isLocal) {
            // Find dir path
            const parentFolders = await recurseFindParents(fileId, user.id)
            let fielPath = parentFolders.reduce((path,  _folder) => {
                return path + _folder.name + '/'
            }, "");
            // TODO: In case of music file clear the thumbnails as well.
            if(file.kind === "FILE") {
                // remove the last "/";
                fielPath = fielPath.slice(0, -1);
            }
            const dirPath = pathFunc.join(serverConfig.publicpath, '/' + fielPath);
            
            fs.removeSync(dirPath);
            // Find children and delete
        } else {
            // pcloud
            const storage = await services.settings.controllers.storage.getAll({ user: user.id, isActive: true });
            if (file.kind === "FOLDER") {
                await cloudFileManager.deleteFolder(file.thirdPartyData, storage[0])
            } else {
                await cloudFileManager.deleteFile(file.thirdPartyData, storage[0])
                
            }
        }
        return await recurseDelChildren(fileId, user.id);
    } catch (e) {
        _console.error(e);
        throw e;
    }
}

export async function upload(req) {
    try {
        const username = req.user.local.username;
        const destRelativeDir = (req.headers['uploader-file-dir'] || '/')
        const file = await req.file();
        const chunkParams = req.headers;
        const fileName = chunkParams['uploader-file-id'];
        const storage = await services.settings.controllers.storage.getAll({ user: req.user.id, isActive: true });
        const destFileDir = storage.length ? `/tmp_${username}/` :username + destRelativeDir;
        chunkParams.path = destFileDir;
        const uploadedFile = <any>await uploader(file, chunkParams, maxFileSize, maxChunkSize);
        const isLastChunk =
            chunkParams['uploader-chunk-number'] === chunkParams['uploader-chunks-total'];
        let fileData = <any>{};
        if(isLastChunk) {
            const srcfilePath = pathFunc.join(serverConfig.publicpath, destFileDir + fileName);
            const type = await FileType.fromFile(srcfilePath);

            // Check if there is a third party file hosting service to upload the file to.
            let metadata = {}, thirdPartyData = <any> {};
            metadata= {...metadata, ...type}
            if (type && type.mime === "video/x-matroska") {
                // Capture metadata for video file
            } else if (type && type.mime === "audio/mpeg") {
                const musicMeta = await captureMusicMetadata(destFileDir, fileName);
                metadata = {...metadata, ...musicMeta}
            }
            fileData = {
                ...uploadedFile,
                thirdPartyData: {},
                isLocal: true,
                link: '',
                path: destFileDir,
                dir: req.headers['uploader-file-dir'],
                size: 0,
                metadata,
                createdAt: new Date(),
            }
            if (storage.length) {
                const thirdPartyData = await cloudFileManager.upload(srcfilePath, username, destRelativeDir, storage[0]);
                fileData.thirdPartyData = thirdPartyData;
                fileData.isLocal = false;
                fileData.link = thirdPartyData.link;
                fileData.download_link = thirdPartyData.download_link;
                fileData.size = thirdPartyData.size;
                fileData.storage = storage[0];

            }
        } else {
            fileData = {
                chunk: file
            }
        }
    
        return fileData;
    } catch(e) {
        _console.error(e);
        throw e;
    }
}

export async function uploadFromUrl(request, user) {
    const {url, dir, fileId} = request;
    const path = user.local.username + dir;
    const info = await ytdl.getInfo(url)
    const audioFormats = ytdl.filterFormats(info.formats, 'audioonly');
    const format =  ytdl.chooseFormat(audioFormats, { audioBitrate: 160 });
    const stream = await ytdl(url, {
        // quality: 'highestaudio',
        format
    })
    // const deferred = Q.defer();
    const srcFilePath = `${serverConfig.publicpath}/${user.local.username}/${dir}/${info.videoDetails.title}.mp3`;
    await ffmpeg(stream)
        .toFormat("mp3")
        .pipe(fsFile.createWriteStream(srcFilePath));

    const file = {
        dir,
        name: info.videoDetails.title + '.mp3',
        path,
        isLocal: true, 
        parent: fileId,
        metadata: {
            ext: "mp3",
            mime: "audio/mpeg",
            audio: {
                title: info.videoDetails.title,
                artists: [info.videoDetails.author.name],
            }
        }
    }
    const storage = await services.settings.controllers.storage.getAll({ user: user.id, isActive: true });
    if (storage.length) {
        file.isLocal = false;
        await cloudFileManager.upload(srcFilePath, user.local.username, dir, storage[0]);
    }
    // ffmpeg().input(stream)
    //     .audioBitrate(128)
    //     .save(`${serverConfig.publicpath}/${user.local.username}/${dir}/${info.videoDetails.title}.mp3`)
    //     .on('progress', p => {
    //         _console.debug(`Downloaded ${p.targetSize} kb`)
    //     })
    //     .on('end', () => {
    //         const file = {
    //             dir,
    //             name: info.videoDetails.title + '.mp3',
    //             path,
    //             parent: fileId,
    //             metadata: {
    //                 ext: "mp3",
    //                 mime: "audio/mpeg",
    //                 audio: {
    //                     title: info.videoDetails.title,
    //                     artists: [info.videoDetails.author.name],
    //                 }
    //             }
    //         }
    //         deferred.resolve(file)
    //     });
    return file;
    
    // _console.debug(Object.keys(info));
    // _console.debug(Object.keys(info.videoDetails))
    // _console.debug(info.videoDetails.thumbnails)
    // _console.debug(info.formats.map(_format => _format.mimeType).join(','));
    // _console.debug(info.videoDetails.author.name)
}

export async function createFolder(name, path, user) {
    // Check if there is a third party file hosting service to upload the file to.
    const storage = await services.settings.controllers.storage.getAll({ user: user.id, isActive: true });
    if (storage.length) {
        // pcloud
        const username = user.local.username;
        const thirdPartyData = await cloudFileManager.createFolder(username, path + name, storage[0])
        return {
            isLocal: false,
            thirdPartyData
        }
    } else {
        const dirPath =  getDirPath(user.local.username, path) + "/" + name;
        await fs.mkdirpSync(dirPath);
        return {
            isLocal: true
        }
    }

}
