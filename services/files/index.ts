import * as FileControls from './controllers/filesupload.ctrl';
import constants from './constants/index';

export default {
    ...FileControls,
    constants,
};
