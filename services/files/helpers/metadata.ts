const pathFunc = require('path');
import * as logger from 'tracer';
const fsFile = require('fs');
import * as mm from 'music-metadata';
import serverConfig from '../../../config/dev';
import { asyncForEach } from '../../../common/utils/asyncHelpers';
const _console = logger.colorConsole();

export async function captureMusicMetadata(path, fileName) {
    let metadata = {}
    const filePath = pathFunc.join(serverConfig.publicpath, path + fileName);
    const fileExtraData = await mm.parseFile(filePath, {
        duration: true
    });
    metadata = {...metadata, ...{audio: {...fileExtraData.common, ...fileExtraData.format }}};
    const pictures = (<any>metadata).audio.picture;
    //TODO: This will fail for 3rd party file hosting service.
    if(pictures) {
        await asyncForEach(pictures, async function(_picture){
            const ext = _picture.format.split('/')[1];
            const _filePath = filePath + '.' + ext;
            await fsFile.writeFileSync(_filePath, _picture.data);
        });
        (<any>metadata).audio.picture =  pictures.map(_pic => {
            const ext = _pic.format.split('/')[1];
            _pic.url = path + '.' + ext;
            return _pic;
        })
    }
    return metadata;
}