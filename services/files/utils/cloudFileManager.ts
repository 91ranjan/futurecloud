import pcloud from './pcloud';
const fs = require('fs');
import * as logger from 'tracer';
const _console = logger.colorConsole();

async function upload(srcfilePath, userDirName, destFileDir, cloudStorage) {
    let file;
    if (cloudStorage.service === 'pcloud') {
        file = await pcloud.upload(
            cloudStorage.config.access_token,
            srcfilePath,
            userDirName,
            destFileDir
        );
    }
    setTimeout(() => {
        fs.rmSync(srcfilePath, {
            force: true,
        });
    }, 5000);
    return file;
}

async function downloadLink(metadata, cloudStorage) {
    if (cloudStorage.service === 'pcloud') {
        return await pcloud.getDownloadLink(
            cloudStorage.config.access_token,
            metadata.fileid,
            metadata.code
        );
    }
    return null;
}

async function downloadAudioLink(metadata, cloudStorage) {
    if (cloudStorage.service === 'pcloud') {
        return await pcloud.audioDownloadLink(cloudStorage.config.access_token, metadata.fileid);
    }
    return null;
}

async function downloadRequestLink(metadata, cloudStorage) {
    if (cloudStorage.service === 'pcloud') {
        return {
            host: 'https://api.pcloud.com/',
            path: `getfilelink`,
            params: {
                forcedownload: 1,
                fileid: metadata.fileid,
                // access_token: cloudStorage.config.access_token,
                code: 'code',
                username: '91Ranjan',
                password: '91@Ranjan',
            },
        };
    }
    return null;
}

async function deleteFile(metadata, cloudStorage) {
    if (cloudStorage.service === 'pcloud') {
        return await pcloud.deleteFile(cloudStorage.config.access_token, metadata.fileid);
    }
    return null;
}

async function deleteFolder(metadata, cloudStorage) {
    if (cloudStorage.service === 'pcloud') {
        return await pcloud.deleteFolder(cloudStorage.config.access_token, metadata.folderid);
    }
    return null;
}

async function createFolder(userIdentifier, path, cloudStorage) {
    if (cloudStorage.service === 'pcloud') {
        return await pcloud.createFolder(cloudStorage.config.access_token, userIdentifier, path);
    }
    return null;
}

export default {
    upload,
    downloadLink,
    downloadAudioLink,
    downloadRequestLink,
    deleteFile,
    deleteFolder,
    createFolder,
};
