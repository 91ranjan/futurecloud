import * as request from 'superagent';
import * as logger from 'tracer';
const _console = logger.colorConsole();
const fs = require('fs');
import * as pcloudSdk from 'pcloud-sdk-js';
import { asyncForEach } from '../../../common/utils/asyncHelpers';
import config from '../../../config/dev';
// @ts-ignore:next-line
global.locationid = 1

const basePath = config.pcloud_base_dir;

export async function getDownloadLink( access_token, fileId, code) {
    const publicLink = await request
            .post(`https://api.pcloud.com/getpublinkdownload?access_token=${access_token}&fileid=${fileId}&forcedownload=1&code=${code}`)
            .then(resp => {
                return JSON.parse(resp.text);
            })
    return `https://${publicLink.hosts[0]}${publicLink.path}`
}

export async function audioDownloadLink(access_token, fileId) {
    const publicLink = await request
            .post(`https://api.pcloud.com/getaudiolink?access_token=${access_token}&fileid=${fileId}&forcedownload=1&abitrate=160`)
            .then(resp => {
                return JSON.parse(resp.text);
            })
    return `https://${publicLink.hosts[0]}${publicLink.path}`
}

export async function deleteFile(access_token, fileId) {
    const client = pcloudSdk.createClient(access_token);
    return await client.deletefile(fileId);
}

export async function deleteFolder(access_token, folderid) {
    const client = pcloudSdk.createClient(access_token);
    return await client.deletefolder(folderid);
}

export async function createFolder(access_token, userDirName, path ) {
    let pcloudPath = basePath + userDirName + path;
    const client = pcloudSdk.createClient(access_token);
    const pathArray = pcloudPath.split('\/').filter(folder => folder);
    let parentFolderId = 0, targetFolder;
    // _console.debug(pcloudPath);
    await asyncForEach(pathArray, async (folder, index) => {
        const files = <any>await client.listfolder(parentFolderId);
        // _console.debug(files);
        const folderExist = files.contents.find(_folder => {
            return _folder.name === folder
        })
        // _console.debug(folderExist )
        if (folderExist) {
            parentFolderId = folderExist.folderid
            if (index === pathArray.length - 1) {
                targetFolder = folderExist
            }
        } else {
            const newFolder = await client.createfolder(folder, parentFolderId);
            // _console.debug(newFolder);
            if (index === pathArray.length - 1) {
                targetFolder = newFolder
            }
        }
    })
    return targetFolder;
}

export async function uploadFileToPcloud(access_token, srcfilePath, userDirName, path) {
    const client = pcloudSdk.createClient(access_token);
    const destFolder = await createFolder(access_token, userDirName, path);
    const uploadedFile = await client.upload(srcfilePath, destFolder.folderid, {});
    const publicLink = await request
            .post(`https://api.pcloud.com/getfilepublink?access_token=${access_token}&fileid=${uploadedFile.metadata.fileid}`)
            .then(resp => {
                return JSON.parse(resp.text);
            })
    const link = await client.getfilelink(uploadedFile.metadata.fileid);
    return {
        name: publicLink.metadata.name ,
        size: publicLink.metadata.size ,
        fileid: publicLink.metadata.fileid,
        code: publicLink.code,
        link: publicLink.link,
        download_link: link, //publicLink.link,
        path: path,
        md5: uploadedFile.checksums.md5,
        sha1: uploadedFile.checksums.sha1,
    }
}

export async function getPublicLink(access_token, fileNumber) {
    const client = pcloudSdk.createClient(access_token);
    return await client.getfilelink(fileNumber);
}

export default { 
    deleteFile,
    deleteFolder,
    upload: uploadFileToPcloud,
    createFolder,
    getDownloadLink,
    audioDownloadLink
}