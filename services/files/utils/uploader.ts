const { promisify } = require('util');
const path = require('path');
const fs = require('fs');
import * as request from "superagent";

import serverconfig from '../../../config/dev';

import pcloud from "./pcloud";
import * as logger from 'tracer';
const _console = logger.colorConsole();

/**
 * Make sure required headers are present & are numbers
 * @param { Object } headers – req.headers object
 * @return { Boolean }
 */
function checkHeaders(headers) {
    if (
        !headers['uploader-chunk-number'] ||
        !headers['uploader-chunks-total'] ||
        !headers['uploader-file-id'] ||
        !headers['uploader-chunks-total'].match(/^[0-9]+$/) ||
        !headers['uploader-chunk-number'].match(/^[0-9]+$/)
    )
        return false;

    return true;
}

/**
 * Make sure total file size isn't bigger than limit
 * @param { Number } maxFileSize
 * @param { Number } maxChunkSize
 * @param { Object } headers – req.headers object
 * @return { Boolean }
 */
function checkTotalSize(maxFileSize, maxChunkSize, totalChunks) {
    if (maxChunkSize * totalChunks > maxFileSize) return false;
    return true;
}

/**
 * Delete tmp directory containing chunks
 * @param { String } dirPath
 */
function cleanChunks(dirPath) {
    fs.readdir(dirPath, (err, files) => {
        let filesLength = files.length;

        files.forEach((file) => {
            fs.unlink(path.join(dirPath, file), () => {
                if (--filesLength === 0) fs.rmdir(dirPath, () => {}); // cb does nothing but required
            });
        });
    });
}

/**
 * Take all chunks of a file and reassemble them in a unique file
 * @param { String } tmpDir
 * @param { String } dirPath
 * @param { String } fileId
 * @param { Number } totalChunks
 * @param { Object } postParams – form post fields
 * @return { Function } promised function to start assembling
 */
function assembleChunks(finalPath, tmpDirPath, fileId, totalChunks, postParams) {
    const asyncReadFile = promisify(fs.readFile);
    const asyncAppendFile = promisify(fs.appendFile);
    const asyncStatFile = promisify(fs.stat);
    const assembledFile = path.join(finalPath, fileId);
    let chunkCount = 0;

    return () => {
        // eslint-disable-line
        return new Promise((resolve, reject) => {
            const pipeChunk = () => {
                asyncReadFile(path.join(tmpDirPath, chunkCount.toString()))
                    .then((chunk) => asyncAppendFile(assembledFile, chunk))
                    .then(async () => {
                        // 0 indexed files = length - 1, so increment before comparison
                        if (totalChunks > ++chunkCount) pipeChunk();
                        else {
                            cleanChunks(tmpDirPath);
                            const combinedFile = await asyncStatFile(assembledFile);
                            resolve({ filePath: finalPath, postParams, file: combinedFile });
                        }
                    })
                    .catch(reject);
            };

            pipeChunk();
        });
    };
}

/**
 * Create directory if it doesn't exist
 * @param { String } dirPath
 * @param { Function } callback
 */
async function mkdirIfDoesntExist(dirPath) {
    const fsStat = promisify(fs.stat);
    const fsMkdir = promisify(fs.mkdir);
    try {
        await fsStat(dirPath)
    } catch(e) {
        await fsMkdir(dirPath, { recursive: true })
    }
}

/**
 * Master function. Parse form and call child ƒs for writing and assembling
 * @param { Object } req – nodejs req object
 * @param { String } tmpDir – upload temp dir
 * @param { Number } maxChunkSize
 */
export default async function uploadFile(file, chunkParams, maxFileSize, maxChunkSize) {
    return new Promise(async (resolve, reject) => {
        const userDirectory = chunkParams.path;
        if (!checkHeaders(chunkParams)) {
            throw new Error('Missing header(s)');
            return;
        }

        if (
            !checkTotalSize(
                maxFileSize,
                chunkParams['uploader-chunks-total'],
                Math.ceil(maxFileSize / maxChunkSize)
            )
        ) {
            throw new Error('File is above size limit');
        }

        try {
            const postParams = {};
            const tmpDir = serverconfig.tempdir;
            // Handling file
            const dirPath = path.join(tmpDir, `${chunkParams['uploader-file-id']}_tmp`);
            const chunkPath = path.join(dirPath, chunkParams['uploader-chunk-number']);
            const userdirPath = path.join(serverconfig.publicpath, userDirectory);
            const chunkCount = +chunkParams['uploader-chunk-number'];
            const totalChunks = +chunkParams['uploader-chunks-total'];
            function writeFile() {
                let writeStream = fs.createWriteStream(chunkPath);

                writeStream.on('error', (err) => {
                    _console.log(err);
                });

                writeStream.on('close', async () => {
                    if (chunkCount === totalChunks) {
                        const resp = await assembleChunks(
                            userdirPath,
                            dirPath,
                            chunkParams['uploader-file-id'],
                            totalChunks,
                            postParams
                        )();
                        const response = {
                            file: (<any>resp).file,
                        }
                        // (<any>resp).path = userDirectory
                        resolve(response);
                    } else {
                        resolve({
                            file
                        });
                    }
                });

                file.file.pipe(writeStream);
            }
            if (chunkCount === 0) {
                await mkdirIfDoesntExist(dirPath)
                await mkdirIfDoesntExist(userdirPath)
                writeFile()
            } else {
                writeFile();
            }
        } catch (err) {
            throw err;
        }
    });
}