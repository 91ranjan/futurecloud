import database from './database';
import users from './users';
import calendar from './calendar';
import tasks from './tasks';
import files from './files';
import music from './music';
import video from './videos';
import finance from './finance';
import scheduler from './scheduler';
import conference from './conference';
import trips from './trip';
import journals from './journals';
import dashboards from './dashboard';
import contacts from './contacts';
import events from './events';
import settings from './settings';

export default {
    events,
    database,
    users,
    calendar,
    tasks,
    files,
    music,
    video,
    finance,
    scheduler,
    conference,
    trips,
    journals,
    dashboards,
    contacts,
    settings,
};
