import * as logger from 'tracer';
const _console = logger.colorConsole();

import services from '../../index';

export async function getAll(filters) {
    try {
        return await services.database.calendar.events.getAll(filters);
    } catch (e) {
        throw e;
    }
}

export async function count(filters) {
    try {
        return await services.database.calendar.events.getCount(filters);
    } catch (e) {
        throw e;
    }
}

export async function getById(id) {
    try {
        services.database.calendar.events.getById(id);
    } catch (e) {
        throw e;
    }
}

export async function create(event) {
    try {
        return await services.database.calendar.events.create(event);
    } catch (e) {
        throw e;
    }
}

export async function update(event) {
    try {
        return await services.database.calendar.events.update(event);
    } catch (e) {}
}

export async function del(request) {
    const { id, organizer } = request;
    try {
        return await services.database.calendar.events.delete({ id, organizer });
    } catch (e) {
        throw e;
    }
}

export default {
    getAll,
    count,
    getById,
    create,
    update,
    del,
};
