import * as logger from 'tracer';
const _console = logger.colorConsole();

import services from '../../index';

export async function getAll(filters) {
    try {
        return await services.database.calendar.calendar.getAll(filters);
    } catch (e) {
        throw e;
    }
}

export async function count(filters) {
    try {
        return await services.database.calendar.calendar.getCount(filters);
    } catch (e) {
        throw e;
    }
}

export async function getById(id) {
    try {
        return await services.database.calendar.calendar.getById(id);
    } catch (e) {
        throw e;
    }
}

export async function create(calendar) {
    try {
        return await services.database.calendar.calendar.create(calendar);
    } catch (e) {
        throw e;
    }
}

export async function update(calendar) {
    try {
        return await services.database.calendar.calendar.update(calendar);
    } catch (e) {
        throw e;
    }
}

export async function del(calendar) {
    try {
        return await services.database.calendar.calendar.delete(calendar);
    } catch (e) {
        throw e;
    }
}

export default {
    getAll,
    count,
    getById,
    create,
    update,
    del,
};
