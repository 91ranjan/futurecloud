import events from './controllers/events.ctrl';
import list from './controllers/calendar.ctrl';

export default {
    controllers: {
        events,
        list
    },
};
