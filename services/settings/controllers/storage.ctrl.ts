import * as logger from 'tracer';
import * as moment from 'moment';
import { asyncForEach } from '../../../common/utils/asyncHelpers';
import { getRecurringDates } from '../../tasks/controllers/boardtasks.ctrl';
const _console = logger.colorConsole();

import services from '../../index';

export async function getAll(filters) {
    try {
        return await services.database.settings.storage.getAll(filters);
    } catch (e) {
        throw e;
    }
}

export async function count(filters) {
    try {
        return await services.database.settings.storage.getCount(filters);
    } catch (e) {
        throw e;
    }
}

export async function getById(id, user) {
    try {
        return await services.database.settings.storage.getById(id, user);
    } catch (e) {
        throw e;
    }
}

export async function create(storage) {
    try {
        return await services.database.settings.storage.create(storage);
    } catch (e) {
        throw e;
    }
}

export async function update(storage, user) {
    try {
        return await services.database.settings.storage.update(storage, user);
    } catch (e) {}
}

export async function del(request, user) {
    const { id } = request;
    try {
        return await services.database.settings.storage.delete({ id }, user);
    } catch (e) {
        throw e;
    }
}

export default {
    getAll,
    count,
    getById,
    create,
    update,
    del,
};
