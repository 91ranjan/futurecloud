import storageCtrl from './controllers/storage.ctrl';
import moduleCtrl from './controllers/module.ctrl';

export default {
    controllers: {
        storage: storageCtrl,
        module: moduleCtrl,
    },
};
