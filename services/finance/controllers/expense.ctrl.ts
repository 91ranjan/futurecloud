import * as logger from 'tracer';
import * as moment from 'moment';
import { asyncForEach } from '../../../common/utils/asyncHelpers';
import { getRecurringDates } from '../../tasks/controllers/boardtasks.ctrl';
const _console = logger.colorConsole();

import services from '../../index';

export async function getAll(filters) {
    try {
        return await services.database.finance.expense.getAll(filters);
    } catch (e) {
        throw e;
    }
}

export async function count(filters) {
    try {
        return await services.database.finance.expense.getCount(filters);
    } catch (e) {
        throw e;
    }
}

export async function getById(id, user) {
    try {
        return await services.database.finance.expense.getById(id, user);
    } catch (e) {
        throw e;
    }
}

export async function create(event) {
    try {
        return await services.database.finance.expense.create(event);
    } catch (e) {
        throw e;
    }
}

export async function update(event, user) {
    try {
        return await services.database.finance.expense.update(event, user);
    } catch (e) {}
}

export async function schedule(data) {
    try {
        let { from_date, repeat, every, to_date, on, user, expense } = data;
        let expenseData = await services.finance.controllers.expenses.getById(expense, user);
        let dates: any[] = getRecurringDates(from_date, repeat, every, to_date, on);
        _console.debug(dates);
        expenseData = expenseData.toObject();
        delete expenseData._id;
        delete expenseData.createdAt;
        delete expenseData.updatedAt;
        delete expenseData.__v;

        // Create board tasks
        await asyncForEach(dates, async (_date) => {
            const momentDate = moment(_date, 'MM-DD-YYYY');
            const expenseItem = {
                ...expenseData,
                date: momentDate.format(),
                status: 'pending',
                paidBy: '',
                user: user,
            };
            await services.database.finance.expense.create(expenseItem);
        });

        return {};
        // return await services.database.tasks.boardtasks.create(board);
    } catch (e) {
        throw e;
    }
}

export async function del(request, user) {
    const { id } = request;
    try {
        return await services.database.finance.expense.delete({ id }, user);
    } catch (e) {
        throw e;
    }
}

export default {
    getAll,
    count,
    getById,
    create,
    update,
    del,
    schedule,
};
