import expensesCtrl from './controllers/expense.ctrl';

export default {
    controllers: { expenses: expensesCtrl },
};
