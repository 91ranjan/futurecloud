import auth from './controllers/auth.ctrl'
import users from './controllers/users.ctrl'

export default {
    controllers : {
        auth,
        users
    }
}