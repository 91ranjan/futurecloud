import * as logger from 'tracer';

import services from '../../index';

export async function get(request) {
    try {
    } catch (e) {
        throw e;
    }
}

export async function getById(id) {
    try {
        return await services.database.user.list.getById(id);
    } catch (e) {
        throw e;
    }
}

export async function create(request) {
    try {
    } catch (e) {
        throw e;
    }
}

export async function update(request) {
    try {
        return await services.database.user.list.update(request);
    } catch (e) {
        throw e;
    }
}

export async function del(request) {
    try {
    } catch (e) {
        throw e;
    }
}

export default {
    get,
    getById,
    create,
    update,
    del,
};
