import * as logger from 'tracer';
import * as jwt from "jsonwebtoken";
import { RequestType } from '../../../common/constants/request.constant';
import mainAppConfig from '../../../config/dev'
const _console = logger.colorConsole();

import services from '../../index';

export async function login(creds = {}) {
    try {
        const {username, email, password} = (<any>creds);
        const user = await services.database.user.list.validateUser({username, email, password});
        if(user) {
            var token = jwt.sign(
                Object.assign({}, user),
                mainAppConfig.session_secret,
                {
                    expiresIn: 1440 * 60 * 60 // expires in 24 hours
                }
            );
            return {
                token,
                user
            }
        } else {
            return false;
        }
    } catch(e) {
        throw e;
    }
}

export async function signup(user) {
    // TODO: Any validation of the user object.
    try{
        const newUser = await services.database.user.list.create(user);
        var token = jwt.sign(
            Object.assign({}, newUser),
            mainAppConfig.session_secret,
            {
                expiresIn: 1440 * 60 * 60 // expires in 24 hours
            }
        );
        return {
            user: newUser,
            token
        }
    } catch(e) {
        throw e;
    }
}
export async function reset_password(request) {}
export async function update_password(request) {}
export async function del(request) {}

export default {
    login,
    signup,
    reset_password,
    update_password,
    del
}