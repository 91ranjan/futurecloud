import taskbucketCtrl from './controllers/taskbucket.ctrl';
import tasksCtrl from './controllers/tasks.ctrl';
import boardtasksCtrl from './controllers/boardtasks.ctrl';
import tasklogCtrl from './controllers/tasks_logs.ctrl';
import taskBlocksCtrl from './controllers/tasksblocks.ctrl';

export default {
    controllers: {
        taskbucket: taskbucketCtrl,
        tasks: tasksCtrl,
        boardtasksCtrl: boardtasksCtrl,
        tasklogCtrl: tasklogCtrl,
        taskBlocksCtrl: taskBlocksCtrl,
    },
};
