import * as logger from 'tracer';
import * as moment from 'moment';
import { asyncForEach } from '../../../common/utils/asyncHelpers';
const _console = logger.colorConsole();

import services from '../../index';

export async function getAll(filters) {
    try {
        return await services.database.tasks.tasks.getAll(filters);
    } catch (e) {
        throw e;
    }
}

export async function count(filters) {
    try {
        return await services.database.tasks.tasks.getCount(filters);
    } catch (e) {
        throw e;
    }
}

export async function getById(id, user) {
    try {
        return await services.database.tasks.tasks.getById(id, user);
    } catch (e) {
        throw e;
    }
}

export async function create(task) {
    try {
        if (!task.parent_task) {
            delete task.parent_task;
        }
        _console.debug(task);
        return await services.database.tasks.tasks.create(task);
    } catch (e) {
        throw e;
    }
}

async function recurseChildrenTasks(tasks, callback) {
    if (!Array.isArray(tasks)) {
        tasks = [tasks];
    }
    return await asyncForEach(tasks, async (_task) => {
        let children = await services.database.tasks.tasks.getAll({
            parent_task: _task.id,
            limit: 1000,
            page: 1,
        });
        // Required as we cannot modify the children if it is a mongo object;
        children = children.map((_child) => _child.toJSON());
        if (children.length) {
            await recurseChildrenTasks(children, callback);
        }
        return await callback(_task);
    });
}

async function recalculateForParent(parent_task_id, user) {
    // Updating the parent's completed percentage.
    const otherChildren = await services.database.tasks.tasks.getAll({
        parent_task: parent_task_id,
        page: 1,
        limit: 1000,
    });
    const totalCompleted =
        otherChildren.reduce((total, _child) => (_child.completed_per || 0) + total, 0) /
        otherChildren.length;
    if (parent_task_id) {
        // Update the parent task
        let parentTask = await services.database.tasks.tasks.getById(parent_task_id, user);
        parentTask = parentTask.toJSON();
        parentTask.completed_per = totalCompleted;
        await services.database.tasks.tasks.update(parentTask, user);
        if (parentTask.parent_task) {
            await recalculateForParent(parentTask.parent_task.id, user);
        }
    }
}

export async function update(task, user) {
    try {
        let returnTask;
        const dbTask = await services.database.tasks.tasks.getById(task.id, user);
        if (task.log) {
            // Log a message for this update
            await services.database.tasks.tasklogs.create({
                description: task.log,
                start_date: moment().subtract(10, 'minutes').toDate(),
                end_date: moment().toDate(),
                task: task.id,
                user,
            });
        }
        if ((dbTask.completed_per || 0) < 100 && task.completed_per === 100) {
            //parent task has been marked as completed so the children should be toolbar.
            returnTask = await recurseChildrenTasks(task, async (_task) => {
                _task.completed_per = 100;
                return await services.database.tasks.tasks.update(_task, user);
            });
        } else if ((dbTask.completed_per || 0) === 100 && task.completed_per < 100) {
            // Mark the parents as not completed.
            // First update the task itself then recurse parents to update the percentage completed.
            returnTask = await services.database.tasks.tasks.update(task, user);
            await recalculateForParent(task.parent_task?.id, user);
        } else if (dbTask.completed_per !== task.completed_per) {
            // First update the task itself then recurse parents to update the percentage completed.
            returnTask = await services.database.tasks.tasks.update(task, user);
            await recalculateForParent(task.parent_task?.id, user);
        } else {
            returnTask = await services.database.tasks.tasks.update(task, user);
        }

        return returnTask;
    } catch (e) {
        throw e;
    }
}

export async function del(task, user) {
    try {
        const deletedTasks = await recurseChildrenTasks(task, async (_task) => {
            return await services.database.tasks.tasks.delete({ id: _task.id }, user);
        });
        return deletedTasks[0];
    } catch (e) {
        throw e;
    }
}

export default {
    getAll,
    count,
    getById,
    create,
    update,
    del,
};
