import * as logger from 'tracer';
import { asyncForEach } from '../../../common/utils/asyncHelpers';
const _console = logger.colorConsole();

import services from '../../index';

export async function getAll(filters) {
    try {
        return await services.database.tasks.taskBlocks.getAll(filters);
    } catch (e) {
        throw e;
    }
}

export async function count(filters) {
    try {
        return await services.database.tasks.taskBlocks.getCount(filters);
    } catch (e) {
        throw e;
    }
}

export async function getById(id, user) {
    try {
        return await services.database.tasks.taskBlocks.getById(id, user);
    } catch (e) {
        throw e;
    }
}

export async function create(task) {
    try {
        return await services.database.tasks.taskBlocks.create(task);
    } catch (e) {
        throw e;
    }
}

export async function update(task, user) {
    try {
        return await services.database.tasks.taskBlocks.update(task, user);
    } catch (e) {
        throw e;
    }
}

export async function del(task, user) {
    try {
        return await services.database.tasks.taskBlocks.delete(
            task.id,
            user
        );
    } catch (e) {
        throw e;
    }
}

export default {
    getAll,
    count,
    getById,
    create,
    update,
    del,
};
