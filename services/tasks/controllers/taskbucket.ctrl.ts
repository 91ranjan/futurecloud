import * as logger from 'tracer';
const _console = logger.colorConsole();

import services from '../../index';

export async function getAll(filters) {
    try {
        return await services.database.tasks.tasksbucket.getAll(filters);
    } catch (e) {
        throw e;
    }
}

export async function count(filters) {
    try {
        return await services.database.tasks.tasksbucket.getCount(filters);
    } catch (e) {
        throw e;
    }
}

export async function getById(id, user) {
    try {
        return await services.database.tasks.tasksbucket.getById(id, user);
    } catch (e) {
        throw e;
    }
}

export async function create(bucket) {
    try {
        return await services.database.tasks.tasksbucket.create(bucket);
    } catch (e) {
        throw e;
    }
}

export async function update(bucket, user) {
    try {
        return await services.database.tasks.tasksbucket.update(bucket, user);
    } catch (e) {
        throw e;
    }
}

export async function del(bucket, user) {
    try {
        return await services.database.tasks.tasksbucket.delete(bucket.id, user);
    } catch (e) {
        throw e;
    }
}

export default {
    getAll,
    count,
    getById,
    create,
    update,
    del,
};
