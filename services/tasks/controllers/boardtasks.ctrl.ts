import * as mongoose from 'mongoose';
import * as logger from 'tracer';
import * as moment from 'moment';
import 'moment-recur-ts';
import { asyncForEach } from '../../../common/utils/asyncHelpers';
const _console = logger.colorConsole();

import services from '../../index';
import { daysOfWeek, monthOfYear, repeatMap } from '../constants/calendarConstants';

export function getRecurringDates(from_date, repeat, every, to_date, on) {
    let dates: any[] = [];
    from_date = moment(from_date).add(1, 'days');
    to_date = moment(to_date);
    let recur = moment().recur(from_date, to_date);
    if (repeat === 'weekly') {
        on = on.map((_num) => daysOfWeek[_num]);
        dates = recur.every(on).daysOfWeek().all('L');
        const startingWeek = moment(dates[0], 'MM-DD-YYYY').week();
        dates = dates.filter((_date) => {
            const currentWeek = moment(_date, 'MM-DD-YYYY').week();
            if ((currentWeek - startingWeek) % every !== 0) {
                return false;
            }
            return true;
        });
    } else if (repeat === 'monthly') {
        on = parseInt(on, 10);
        dates = recur.every(on).dayOfMonth().all('L');
        const startingMonth = moment(dates[0], 'MM-DD-YYYY').month();
        dates = dates.filter((_date) => {
            const currentMonth = moment(_date, 'MM-DD-YYYY').month();
            if ((currentMonth - startingMonth) % every !== 0) {
                return false;
            }
            return true;
        });
    } else if (repeat === 'yearly') {
        on = parseInt(on, 10);
        every = every.map((_month) => monthOfYear[_month]);
        dates = recur.every(on).daysOfMonth().every(every).monthsOfYear().all('L');
    } else {
        dates = recur.every(every, repeatMap[repeat]).all('L');
    }
    return dates;
}

export async function getAll(filters) {
    try {
        return await services.database.tasks.boardtasks.getAll(filters);
    } catch (e) {
        throw e;
    }
}

export async function count(filters) {
    try {
        return await services.database.tasks.boardtasks.getCount(filters);
    } catch (e) {
        throw e;
    }
}

export async function getById(id, user) {
    try {
        return await services.database.tasks.boardtasks.getById(id, user);
    } catch (e) {
        throw e;
    }
}

export async function create(board) {
    try {
        return await services.database.tasks.boardtasks.create(board);
    } catch (e) {
        throw e;
    }
}

export async function schedule(data) {
    try {
        let { taskId, from_date, repeat, every, to_date, on, user, start_date, end_date, slotId } =
            data;
        let dates: any[] = getRecurringDates(from_date, repeat, every, to_date, on);
        _console.debug(dates);

        start_date = start_date ? moment(start_date) : null;
        end_date = end_date ? moment(end_date) : null;
        // Create board tasks
        await asyncForEach(dates, async (_date) => {
            const momentDate = moment(_date, 'MM-DD-YYYY');
            const boardTask = <any>{
                date: _date,
                isActive: false,
                lastActive: null,
                time_logged: 0,
                status: 'PENDING',
                task: taskId,
                slotId,
                user,
                
            };
            if (start_date) {
                boardTask.start_date = momentDate
                    .hour(start_date.hour())
                    .minute(start_date.minutes())
                    .toDate();
                }
            if (end_date) {
                boardTask.end_date = momentDate.hour(end_date.hour()).minute(end_date.minutes()).toDate()
            }
            await services.database.tasks.boardtasks.create(boardTask);
        });

        return {};
        // return await services.database.tasks.boardtasks.create(board);
    } catch (e) {
        throw e;
    }
}

export async function update(board, user) {
    try {
        if (board.log) {
            // Log a message for this update
            await services.database.tasks.tasklogs.create({
                description: board.log,
                start_date: moment().subtract(10, 'minutes').toDate(),
                end_date: moment().toDate(),
                task: board.task,
                user,
            });
        }
        return await services.database.tasks.boardtasks.update(board, user);
    } catch (e) {
        throw e;
    }
}

export async function del(board, user) {
    try {
        const { id, slotId } = board;
        let deletedTasks;
        if (!slotId) {
            deletedTasks = await services.database.tasks.boardtasks.delete(
                {
                    id,
                },
                user
            );
        } else {
            deletedTasks = await services.database.tasks.boardtasks._model.updateMany(
                { slotId, user },
                {
                    $set: { status: 'DELETED' },
                }
            );
        }
        return deletedTasks[0];
    } catch (e) {
        throw e;
    }
}

export default {
    getAll,
    count,
    getById,
    create,
    update,
    schedule,
    del,
};
