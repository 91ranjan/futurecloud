import { EventEmitter } from 'events';

var eventEmitter = new EventEmitter();

export function broadcastEvent(type, metadata) {
    eventEmitter.emit(type, metadata);
}

export function listenToEvent(type, cb) {
    eventEmitter.on(type, function secondListener(metadata) {
        cb(metadata);
    });
}

export default {
    broadcastEvent,
    listenToEvent,
};
