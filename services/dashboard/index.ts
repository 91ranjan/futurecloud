import dashboardsCtrls from './controllers/dashboards.ctrl';

export default {
    controllers: {
        list: dashboardsCtrls,
    },
};
