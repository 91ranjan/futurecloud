import roomsCtrl from './controllers/rooms.ctrl';
// import musicPlaylistCtrl from './controllers/music_playlist.ctrl';
// import * as musicConstants from './constants/music.contant';

export default {
    controllers: { rooms: roomsCtrl },
    // constants: musicConstants,
};
