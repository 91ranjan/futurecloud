import * as logger from 'tracer';
import { asyncForEach } from '../../../common/utils/asyncHelpers';
const _console = logger.colorConsole();

import services from '../../index';

export async function getAll(filters) {
    try {
        return await services.database.conference.rooms.getAll(filters);
    } catch (e) {
        throw e;
    }
}

export async function count(filters) {
    try {
        return await services.database.conference.rooms.getCount(filters);
    } catch (e) {
        throw e;
    }
}

export async function getById(id) {
    try {
        return await services.database.conference.rooms.getById(id);
    } catch (e) {
        throw e;
    }
}

export async function getByRoomId(roomId) {
    try {
        return await services.database.conference.rooms.getByRoomId(roomId);
    } catch (e) {
        throw e;
    }
}

export async function create(room) {
    try {
        return await services.database.conference.rooms.create(room);
    } catch (e) {
        throw e;
    }
}

export async function update(room) {
    try {
        return await services.database.conference.rooms.update(room);
    } catch (e) {
        throw e;
    }
}

export async function del(request) {
    const { id } = request;
    try {
        return await services.database.conference.rooms.delete({ id });
    } catch (e) {
        throw e;
    }
}

export default {
    getAll,
    count,
    getById,
    getByRoomId,
    create,
    update,
    del,
};
