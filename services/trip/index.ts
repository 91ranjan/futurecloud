import tripsCtrls from './controllers/trips.ctrl';
import tripPlacesCtrls from './controllers/trips_places.ctrl';
import tripLodgeCtrl from './controllers/trips_lodge.ctrl';
import tripExpenseCtrl from './controllers/trips_expense.ctrl';
import tripCommuteCtrl from './controllers/trips_commute.ctrl';
import tripChecklistCtrl from './controllers/trips_checklist.ctrl';
import tripNotesCtrl from './controllers/trips_notes.ctrl';
import tripActivityCtrl from './controllers/trips_activity.ctrl';

export default {
    controllers: {
        list: tripsCtrls,
        places: tripPlacesCtrls,
        lodge: tripLodgeCtrl,
        expense: tripExpenseCtrl,
        commute: tripCommuteCtrl,
        checklist: tripChecklistCtrl,
        notes: tripNotesCtrl,
        activity: tripActivityCtrl,
    },
};
