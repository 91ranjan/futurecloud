import * as logger from 'tracer';
import * as moment from 'moment';
import { asyncForEach } from '../../../common/utils/asyncHelpers';
import { getRecurringDates } from '../../tasks/controllers/boardtasks.ctrl';
const _console = logger.colorConsole();

import services from '../../index';

export async function getAll(filters) {
    try {
        return await services.database.trips.commute.getAll(filters);
    } catch (e) {
        throw e;
    }
}

export async function count(filters) {
    try {
        return await services.database.trips.commute.getCount(filters);
    } catch (e) {
        throw e;
    }
}

export async function getById(id, user) {
    try {
        return await services.database.trips.commute.getById(id, user);
    } catch (e) {
        throw e;
    }
}

export async function create(event) {
    try {
        return await services.database.trips.commute.create(event);
    } catch (e) {
        throw e;
    }
}

export async function update(event, user) {
    try {
        return await services.database.trips.commute.update(event, user);
    } catch (e) {}
}

export async function del(request, user) {
    const { id } = request;
    try {
        return await services.database.trips.commute.delete({ id }, user);
    } catch (e) {
        throw e;
    }
}

export default {
    getAll,
    count,
    getById,
    create,
    update,
    del,
};
