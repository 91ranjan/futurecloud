import * as mongoose from 'mongoose';
import * as bluebird from 'bluebird';

import activity from './models/activity';
import user from './models/users';
import calendar from './models/calendar';
import tasks from './models/tasks';
import files from './models/files';
import music from './models/music';
import videos from './models/videos';
import conference from './models/conference';
import finance from './models/finance';
import trips from './models/trips';
import journals from './models/journals';
import dashboard from './models/dashboard';
import contacts from './models/contacts';
import settings from './models/settings';

import serverconfig from '../../config/dev';
import * as logger from 'tracer';
const _console = logger.colorConsole();

// Connecting to the database
// 1. Checking if db is up or not
(<any>mongoose).Promise = bluebird;
mongoose.connect(serverconfig.dbUrl, { useNewUrlParser: true, useUnifiedTopology: true });
const db = mongoose.connection;
db.on('error', error => {
    _console.error('connection error:');
    _console.error(error);
})
db.once('open', function () {
    _console.log('Connected to database');
})

export default {
    activity,
    user,
    calendar,
    tasks,
    files,
    music,
    videos,
    conference,
    finance,
    trips,
    journals,
    dashboard,
    contacts,
    settings
};
