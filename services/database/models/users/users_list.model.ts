import * as mongoose from 'mongoose';
import * as bcrypt from 'bcryptjs';
import * as logger from 'tracer';
import { getListFilters } from '../../helpers/pageHelpers';

const _console = logger.colorConsole();

const Q = require('q');

const filterOpts = {
    like: ['local.email'],
};

const generateHash = function (password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

class UsersModel {
    _schema;
    _model;
    constructor() {
        this._init();
    }
    _init() {
        this._defineSchema();
        this._initModel();
    }

    _defineSchema() {
        this._schema = new mongoose.Schema(
            {
                // id: { type: String, required: true },
                local: {
                    id: String,
                    username: { type: String, required: true, unique: true },
                    first_name: { type: String, required: false },
                    last_name: { type: String, required: false },
                    email: { type: String, required: true, unique: true },
                    password: { type: String, required: false },
                    mobile: { type: String, required: true, unique: true },
                    address: { type: String, required: false, unique: false },
                    country: { type: String, required: false, unique: false },
                    state: { type: String, required: false, unique: false },
                    dob: { type: Date, required: false, unique: false },
                    role: { type: Number, default: 0 }, // 1 -> admin , 2 -> User
                    resetPasswordToken: String,
                    resetPasswordExpires: Date,
                },
            },
            {
                timestamps: true,
                toObject: { virtuals: true },
                toJSON: { virtuals: true },
            }
        );
        this._schema.virtual('fullname').get(function () {
            return [this.local.first_name, this.local.last_name].filter(Boolean).join(' ');
        });
        this._schema.methods.getUsername = function () {
            return this.local.email;
        };
        this._schema.methods.getPassword = function () {
            return this.local.password;
        };
        this._schema.methods.validPassword = function (password) {
            if (password && this.getPassword()) {
                // the code breaks if either of the arguments are undefined.
                return bcrypt.compareSync(password, this.getPassword());
            } else {
                return false;
            }
        };
        this._schema.methods.getPublicFields = function () {
            var returnObject = this;
            try {
                returnObject = {
                    first_name: this.local.first_name,
                    id: this._id,
                    _id: this._id,
                    last_name: this.local.last_name,
                    email: this.local.email,
                    mobile: this.local.mobile,
                };
            } catch (e) {
                _console.error(e);
            }
            return returnObject;
        };
    }

    _initModel() {
        this._model = mongoose.model('users', this._schema);
    }

    validateUser(filters) {
        var deferred = Q.defer();
        this._model.findOne(
            {
                $or: [
                    {
                        'local.email': filters.email.trim(),
                    },
                    {
                        'local.username': filters.email.trim(),
                    },
                ],
            },
            function (err, user) {
                if (err) return deferred.reject(new Error(err));
                if (!user) return deferred.reject(new Error('Username or email does not exist'));
                if (!user.validPassword(filters.password)) {
                    return deferred.reject(new Error('Invalid Credentials'));
                }
                const userJSON = user.toJSON();
                delete userJSON.local.password;
                return deferred.resolve(userJSON);
            }
        );
        return deferred.promise;
    }

    getAll(filters) {
        var deferred = Q.defer();
        const { searchFilters, pageFilters } = getListFilters(filters, filterOpts);
        this._model
            .find(searchFilters)
            .limit(pageFilters.limit)
            .skip(pageFilters.offset)
            .sort({ createdAt: -1 })
            .select('-local.password')
            .exec(function (err, docs) {
                if (err) {
                    _console.log('Features.getAll : ' + err.message);
                    return deferred.reject(new Error(err));
                }
                return deferred.resolve(docs || []);
            });

        return deferred.promise;
    }

    getCount(filters) {
        var deferred = Q.defer();
        const { searchFilters } = getListFilters(filters, filterOpts);

        this._model.countDocuments(searchFilters).exec(function (err, docs) {
            if (err) {
                _console.error(err);
                return deferred.reject(new Error(err));
            }
            return deferred.resolve(docs);
        });

        return deferred.promise;
    }

    getById(id) {
        var deferred = Q.defer();
        this._model
            .findOne({ _id: id })
            .select('-local.password')
            .exec(function (err, docs) {
                if (err) {
                    _console.log('Features.getById : ' + err.message);
                    return deferred.reject(new Error(err));
                }
                return deferred.resolve(docs || {});
            });

        return deferred.promise;
    }

    getByEmail(email) {
        var deferred = Q.defer();
        this._model.findOne({ email }).exec(function (err, docs) {
            if (err) {
                _console.log('Features.getById : ' + err.message);
                return deferred.reject(new Error(err));
            }
            return deferred.resolve(docs);
        });

        return deferred.promise;
    }

    create(data) {
        const deferred = Q.defer();
        const { first_name, username, last_name, email, password, mobile, role } = data;
        var newUser = new this._model({
            'local.username': username,
            'local.first_name': first_name,
            'local.last_name': last_name,
            'local.email': email,
            'local.password': generateHash(password),
            'local.mobile': mobile,
            'local.role': role,
        });

        newUser.save((err, user) => {
            if (err) {
                _console.log('Failed to save user.');
                return deferred.reject(new Error(err));
            }
            return deferred.resolve(user);
        });

        return deferred.promise;
    }

    async update(data) {
        if (data.local.password) {
            data.local.password = generateHash(data.local.password);
        } else {
            const user = await this._model.findOne({ _id: data._id });
            data.local.password = user.local.password;
        }

        var deferred = Q.defer();
        this._model
            .findOneAndUpdate({ _id: data._id }, data, { new: true, upsert: true })
            .select('-local.password')
            .exec((err, doc) => {
                if (err) {
                    return deferred.reject(new Error(err));
                }
                deferred.resolve(doc);
            });

        return deferred.promise;
    }

    resetPassword(id, email, password) {
        var deferred = Q.defer();
        let local_set = {};

        local_set['local.password'] = generateHash(password);

        this._model.findOneAndUpdate(
            { _id: id, 'local.email': email },
            { $set: local_set },
            { new: true, upsert: true },
            (err, data) => {
                if (err) {
                    _console.log('Failed to update user.');
                    return deferred.reject(new Error(err));
                }
                return deferred.resolve(data);
            }
        );
        return deferred.promise;
    }

    delete(id) {
        var deferred = Q.defer();

        this._model.findOneAndRemove({ _id: id }, (err, doc) => {
            if (err) {
                return deferred.reject(new Error(err));
            }
            deferred.resolve(doc);
        });

        return deferred.promise;
    }
}

export default new UsersModel();
