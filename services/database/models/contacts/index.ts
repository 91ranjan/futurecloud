import list from './contacts.model';
import books from './contact_books.model';

export default {
    list,
    books,
};
