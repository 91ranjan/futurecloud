import * as mongoose from 'mongoose';
import * as bcrypt from 'bcryptjs';
import * as logger from 'tracer';
import * as shortid from 'shortid';
import { getListFilters } from '../../helpers/pageHelpers';

const _console = logger.colorConsole();

const Q = require('q');

const filterOpts = {
    like: ['name'],
};

const generateHash = function (password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

class RoomsModel {
    _schema;
    _model;
    constructor() {
        this._init();
    }
    _init() {
        this._defineSchema();
        this._initModel();
    }

    _defineSchema() {
        this._schema = new mongoose.Schema(
            {
                name: { type: String, required: false },
                host: { type: String, required: true },
                roomId: { type: String, required: true },
                users: [
                    {
                        username: { type: String, required: true },
                        isGuest: { type: Boolean, required: false, default: false },
                    },
                ],
                connections: [
                    {
                        client1: { type: String, required: true },
                        status: { type: String, required: true },
                        client2: { type: String, required: true },
                    },
                ],
                type: { type: String, required: true },
                screen_share: {
                    username: { type: String, required: false },
                },
            },
            {
                timestamps: true,
                toObject: { virtuals: true },
                toJSON: { virtuals: true },
            }
        );
        this._schema.virtual('id').get(function (this: any) {
            return this._id;
        });
    }

    _initModel() {
        this._model = mongoose.model('conference_rooms', this._schema);
    }

    getAll(filters) {
        var deferred = Q.defer();
        const { searchFilters, pageFilters } = getListFilters(filters, filterOpts);
        this._model
            .find(searchFilters)
            .limit(pageFilters.limit)
            .skip(pageFilters.offset)
            .sort({ createdAt: -1 })
            .exec(function (err, docs) {
                if (err) {
                    _console.log('Rooms.getAll : ' + err.message);
                    return deferred.reject(new Error(err));
                }
                return deferred.resolve(docs || []);
            });

        return deferred.promise;
    }

    getCount(filters) {
        var deferred = Q.defer();
        const { searchFilters } = getListFilters(filters, filterOpts);

        this._model.countDocuments(searchFilters).exec(function (err, docs) {
            if (err) {
                _console.error(err);
                return deferred.reject(new Error(err));
            }
            return deferred.resolve(docs);
        });

        return deferred.promise;
    }

    getById(id) {
        var deferred = Q.defer();
        this._model.findOne({ _id: id }).exec(function (err, docs) {
            if (err) {
                _console.log('Rooms.getById : ' + err.message);
                return deferred.reject(new Error(err));
            }
            return deferred.resolve(docs);
        });

        return deferred.promise;
    }

    getByRoomId(roomId) {
        var deferred = Q.defer();
        this._model.findOne({ roomId }).exec(function (err, docs) {
            if (err) {
                _console.log('Rooms.getById : ' + err.message);
                return deferred.reject(new Error(err));
            }
            return deferred.resolve(docs);
        });

        return deferred.promise;
    }

    create(data) {
        const deferred = Q.defer();
        var newData = new this._model(data);
        newData.roomId = shortid.generate();
        _console.log(newData);
        newData.save((err, user) => {
            if (err) {
                _console.log('Failed to save user.');
                return deferred.reject(new Error(err));
            }
            return deferred.resolve(user);
        });

        return deferred.promise;
    }

    async update(data) {
        var deferred = Q.defer();
        this._model
            .findOneAndUpdate({ _id: data._id }, data, {
                new: true,
                upsert: true,
                overwrite: true,
            })
            .exec((err, doc) => {
                if (err) {
                    return deferred.reject(new Error(err));
                }
                deferred.resolve(doc);
            });

        return deferred.promise;
    }

    delete({ id }) {
        var deferred = Q.defer();

        this._model.findOneAndRemove({ _id: id }, (err, doc) => {
            if (err) {
                return deferred.reject(new Error(err));
            }
            deferred.resolve(doc);
        });

        return deferred.promise;
    }
}

export default new RoomsModel();
