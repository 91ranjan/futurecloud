import * as mongoose from 'mongoose';
import * as logger from 'tracer';
import { getListFilters } from "../../helpers/pageHelpers";

const _console = logger.colorConsole();
const Q = require("q");

const filterOpts = {
    like: []
};

class ActivityModel {
    _schema;
    _model;
    _redis;
    constructor() {
        this._init();
    }
    _init() {
        this._defineSchema();
        this._initModel();
    }

    _defineSchema() {
        this._schema = new mongoose.Schema(
            {
                app: { type: String, required: true },
                level: { type: Number, required: true }, // 1: Info,2: Debug, 3:  Warning,4:  Error,5: Fatal
                type: { type: Number, required: true }, // 1: Core, 2: Service, 3: Modules
                title: { type: String, required: true },
                messages: {
                    customMessage: { type: String, required: true },
                    exception: { type: String, required: false },
                    file: { type: String, required: false },
                    line: { type: String, required: false },
                    hint: { type: String, required: false },
                    trace: { type: String, required: false },
                },
                method :{ type: String, required: false },
                remoteAddr :{ type: String, required: false },
                url :{ type: String, required: false },
                user: { type: mongoose.Schema.Types.ObjectId, required: false, ref: 'users' },
                version :{ type: String, required: false },
            },
            {
                timestamps: true,
                toObject: { virtuals: true },
                toJSON: { virtuals: true },
            }
        );
        this._schema.virtual("id").get(function (this: any) {
            return this._id;
        });

        this._schema.statics.bulkInsert = async function (models, fn) {
            if (!models || !models.length)
                return fn(null);

            var bulk = this.collection.initializeOrderedBulkOp();
            if (!bulk)
                return fn('bulkInsertModels: MongoDb connection is not yet established');

            var model;
            for (var i = 0; i < models.length; i++) {
                model = models[i];
                bulk.insert(model.toJSON());
            }

            const docs = await bulk.execute(fn);
            return docs;
        };

        // this._schema.index({ first_name: "text", last_name: "text", common_name: "text", team_name: "text", country_name: "text" });
    }

    _initModel() {
        this._model = mongoose.model("activity_logs", this._schema)
    }

    getAll(filters) {
        var deferred = Q.defer();
        const { pageFilters, searchFilters } = getListFilters(
            filters,
            filterOpts
        );

        this._model.find(searchFilters)
            .limit(pageFilters.limit)
            .skip(pageFilters.offset)
            .sort(pageFilters.sort)
            .exec(function (err, docs) {
                if (err) {
                    (<any>_console).log('Events.getAll : ' + err.message);
                    return deferred.reject(new Error(err));
                }
                return deferred.resolve(docs || {});
            });

        return deferred.promise;
    }

    getCount(filters) {
        var deferred = Q.defer();
        const { searchFilters } = getListFilters(
            filters,
            filterOpts
        );

        this._model.countDocuments(searchFilters).exec(function (err, docs) {
            if (err) {
                _console.error(err);
                return deferred.reject(new Error(err));
            }
            return deferred.resolve(docs);
        });

        return deferred.promise;
    };

    getById(id) {
        var deferred = Q.defer();

        this._model.findOne({ _id: id })
            .exec(function (err, docs) {
                if (err) {
                    _console.log('Player.byId : ' + err.message);
                    return deferred.reject(new Error(err));
                }
                return deferred.resolve(docs || {});
            });

        return deferred.promise;
    }

    create(data) {
        var deferred = Q.defer();
        data.id = mongoose.Types.ObjectId();
        this._model.findOneAndUpdate(
            { _id: data.id },
            data,
            { new: true, upsert: true })
            .exec((err, doc) => {
                if (err) {
                    return deferred.reject(new Error(err));
                }
                deferred.resolve(doc);
            });

        return deferred.promise;
    }

    update(data, filter?) {
        var deferred = Q.defer();

        this._model.findOneAndUpdate(filter || { _id: data.id }, data, { new: true })
            .exec((err, doc) => {
                if (err) {
                    return deferred.reject(new Error(err));
                }
                deferred.resolve(doc);
            });

        return deferred.promise;
    }

    delete(id) {
        var deferred = Q.defer();

        this._model.findOneAndRemove({ _id: id }, (err, doc) => {
            if (err) {
                return deferred.reject(new Error(err));
            }
            deferred.resolve(doc);
        });

        return deferred.promise;
    }
}

export default new ActivityModel();
