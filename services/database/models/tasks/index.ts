import tasksbucket from './taskbucket.model';
import tasks from './tasks.model';
import boardtasks from './board_tasks.model';
import tasklogs from './tasks_log.model';
import taskBlocks from './tasksblock.model';

export default {
    tasksbucket,
    tasks,
    boardtasks,
    tasklogs,
    taskBlocks
};
