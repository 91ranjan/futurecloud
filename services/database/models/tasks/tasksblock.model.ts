import * as mongoose from 'mongoose';
import * as bcrypt from 'bcryptjs';
import * as logger from 'tracer';
import { getListFilters } from '../../helpers/pageHelpers';

const _console = logger.colorConsole();

const Q = require('q');

const filterOpts = {
    like: ['title'],
    limit: 100,
};

class TasksBlockModel {
    _schema;
    _model;
    constructor() {
        this._init();
    }
    _init() {
        this._defineSchema();
        this._initModel();
    }

    _defineSchema() {
        this._schema = new mongoose.Schema(
            {
                title: { type: String, required: true },
                description: { type: String, required: false },
                // Calender attributes
                date: { type: Date, required: true },
                start_date: { type: Date, required: true },
                end_date: { type: Date, required: true },
                bucket: {
                    type: mongoose.Schema.Types.ObjectId,
                    required: false,
                    ref: 'tasks_bucket',
                },
                color: { type: String, required: false },
                user: { type: mongoose.Schema.Types.ObjectId, required: true, ref: 'users' },
            },
            {
                timestamps: true,
                toObject: { virtuals: true },
                toJSON: { virtuals: true },
            }
        );
        this._schema.virtual('id').get(function (this: any) {
            return this._id;
        });
    }

    _initModel() {
        this._model = mongoose.model('tasks_blocks', this._schema);
    }

    getAll(filters) {
        var deferred = Q.defer();
        const { searchFilters, pageFilters } = getListFilters(filters, filterOpts);
        if (searchFilters.bucket) {
            searchFilters.bucket = mongoose.Types.ObjectId(searchFilters.bucket);
        }
        this._model
            .find(searchFilters)
            .populate('user')
            .populate('bucket')
            .limit(pageFilters.limit)
            .skip(pageFilters.offset)
            .sort({ createdAt: -1 })
            .exec(function (err, docs) {
                if (err) {
                    _console.log('TasksBlocks.getAll : ' + err.message);
                    return deferred.reject(new Error(err));
                }
                return deferred.resolve(docs || []);
            });

        return deferred.promise;
    }

    getCount(filters) {
        var deferred = Q.defer();
        const { searchFilters } = getListFilters(filters, filterOpts);

        this._model.countDocuments(searchFilters).exec(function (err, docs) {
            if (err) {
                _console.error(err);
                return deferred.reject(new Error(err));
            }
            return deferred.resolve(docs);
        });

        return deferred.promise;
    }

    getById(id, user) {
        var deferred = Q.defer();
        this._model
            .findOne({ _id: id, user })
            .populate('user')
            .populate('bucket')
            .exec(function (err, docs) {
                if (err) {
                    _console.log('TasksBlocks.getById : ' + err.message);
                    return deferred.reject(new Error(err));
                }
                return deferred.resolve(docs || {});
            });

        return deferred.promise;
    }

    create(data) {
        const deferred = Q.defer();
        var newData = new this._model(data);
        newData.save((err, user) => {
            if (err) {
                _console.log('Failed to save user.');
                return deferred.reject(new Error(err));
            }
            return deferred.resolve(user);
        });

        return deferred.promise;
    }

    async update(data, user) {
        var deferred = Q.defer();
        this._model
            .findOneAndUpdate({ _id: data._id, user }, data, {
                new: true,
                upsert: true,
                overwrite: true,
            })
            .populate('user')
            .populate('bucket')
            .exec((err, doc) => {
                if (err) {
                    return deferred.reject(new Error(err));
                }
                deferred.resolve(doc);
            });

        return deferred.promise;
    }

    delete(id, user) {
        var deferred = Q.defer();

        this._model.findOneAndRemove({ _id: id, user }, (err, doc) => {
            if (err) {
                return deferred.reject(new Error(err));
            }
            deferred.resolve(doc);
        });

        return deferred.promise;
    }
}

export default new TasksBlockModel();
