import * as mongoose from 'mongoose';
import * as bcrypt from 'bcryptjs';
import * as logger from 'tracer';
import { getListFilters } from '../../helpers/pageHelpers';

const _console = logger.colorConsole();

const Q = require('q');

const filterOpts = {
    like: ['title'],
};

class TaskLogsModel {
    _schema;
    _model;
    constructor() {
        this._init();
    }
    _init() {
        this._defineSchema();
        this._initModel();
    }

    _defineSchema() {
        this._schema = new mongoose.Schema(
            {
                description: { type: String, required: false },
                // Calender attributes
                start_date: { type: Date, required: false },
                end_date: { type: Date, required: false },
                board_task: {
                    type: mongoose.Schema.Types.ObjectId,
                    required: false,
                    ref: 'board_tasks',
                },
                task: {
                    type: mongoose.Schema.Types.ObjectId,
                    required: false,
                    ref: 'tasks',
                },
                user: { type: mongoose.Schema.Types.ObjectId, required: true, ref: 'users' },
            },
            {
                timestamps: true,
                toObject: { virtuals: true },
                toJSON: { virtuals: true },
            }
        );
        this._schema.virtual('id').get(function (this: any) {
            return this._id;
        });
    }

    _initModel() {
        this._model = mongoose.model('task_logs', this._schema);
    }

    getAll(filters) {
        var deferred = Q.defer();
        const { searchFilters, pageFilters } = getListFilters(filters, filterOpts);
        this._model
            .find(searchFilters)
            .populate('user')
            .populate('task')
            .populate('board_task')
            .limit(pageFilters.limit)
            .skip(pageFilters.offset)
            .sort({ createdAt: -1 })
            .exec(function (err, docs) {
                if (err) {
                    _console.log('Features.getAll : ' + err.message);
                    return deferred.reject(new Error(err));
                }
                return deferred.resolve(docs || []);
            });

        return deferred.promise;
    }

    getCount(filters) {
        var deferred = Q.defer();
        const { searchFilters } = getListFilters(filters, filterOpts);

        this._model.countDocuments(searchFilters).exec(function (err, docs) {
            if (err) {
                _console.error(err);
                return deferred.reject(new Error(err));
            }
            return deferred.resolve(docs);
        });

        return deferred.promise;
    }

    getById(id, user) {
        var deferred = Q.defer();
        this._model
            .findOne({ _id: id, user })
            .populate('user')
            .populate('task')
            .populate('board_task')
            .exec(function (err, docs) {
                if (err) {
                    _console.log('Features.getById : ' + err.message);
                    return deferred.reject(new Error(err));
                }
                return deferred.resolve(docs || {});
            });

        return deferred.promise;
    }

    create(data) {
        const deferred = Q.defer();
        var newData = new this._model(data);
        newData.save((err, user) => {
            if (err) {
                _console.log('Failed to save user.');
                return deferred.reject(new Error(err));
            }
            return deferred.resolve(user);
        });

        return deferred.promise;
    }

    async update(data, user) {
        var deferred = Q.defer();
        this._model
            .findOneAndUpdate({ _id: data._id, user }, data, {
                new: true,
                upsert: true,
                overwrite: true,
            })
            .populate('user')
            .populate('task')
            .populate('board_task')
            .exec((err, doc) => {
                if (err) {
                    return deferred.reject(new Error(err));
                }
                deferred.resolve(doc);
            });

        return deferred.promise;
    }

    delete({ id, user }) {
        var deferred = Q.defer();

        this._model.findOneAndRemove({ _id: id, user: user.id }, (err, doc) => {
            if (err) {
                return deferred.reject(new Error(err));
            }
            deferred.resolve(doc);
        });

        return deferred.promise;
    }
}

export default new TaskLogsModel();
