import * as mongoose from 'mongoose';
import * as bcrypt from 'bcryptjs';
import * as logger from 'tracer';
import { getListFilters } from '../../helpers/pageHelpers';

const _console = logger.colorConsole();

const Q = require('q');

const filterOpts = {
    like: ['title'],
    limit: 100,
};

class TasksModel {
    _schema;
    _model;
    constructor() {
        this._init();
    }
    _init() {
        this._defineSchema();
        this._initModel();
    }

    _defineSchema() {
        this._schema = new mongoose.Schema(
            {
                title: { type: String, required: true },
                description: { type: String, required: false },
                // Calender attributes
                start_date: { type: Date, required: false },
                end_date: { type: Date, required: false },
                // Schedule a task
                date: { type: Date, required: false },
                deadline: { type: Date, required: false },
                bucket: {
                    type: mongoose.Schema.Types.ObjectId,
                    required: false,
                    ref: 'tasks_bucket',
                },
                status: { type: String, required: false }, // COMPLETED, PENDING
                priority: { type: String, required: false }, // URGENT, HIGH, MEDIUM, LOW
                completed_per: { type: Number, required: false, default: 0 },
                important: { type: Boolean, required: false, default: false },
                all_day: { type: Boolean, required: false, default: false },
                hide_completed: { type: Boolean, required: false, default: false },
                hide_subtasks: { type: Boolean, required: false, default: false },
                frequency: { type: String, required: false, default: 'once' }, // Once, recurring
                parent_task: {
                    type: mongoose.Schema.Types.ObjectId,
                    required: false,
                    ref: 'tasks',
                },
                user: { type: mongoose.Schema.Types.ObjectId, required: true, ref: 'users' },
            },
            {
                timestamps: true,
                toObject: { virtuals: true },
                toJSON: { virtuals: true },
            }
        );
        this._schema.virtual('id').get(function (this: any) {
            return this._id;
        });
    }

    _initModel() {
        this._model = mongoose.model('tasks', this._schema);
    }

    getAll(filters) {
        var deferred = Q.defer();
        const { searchFilters, pageFilters } = getListFilters(filters, filterOpts);
        if (searchFilters.bucket) {
            searchFilters.bucket = mongoose.Types.ObjectId(searchFilters.bucket);
        }
        this._model
            .find(searchFilters)
            .populate('user')
            .populate('parent_task')
            .populate('bucket')
            .limit(pageFilters.limit)
            .skip(pageFilters.offset)
            .sort({ createdAt: -1 })
            .exec(function (err, docs) {
                if (err) {
                    _console.log('Features.getAll : ' + err.message);
                    return deferred.reject(new Error(err));
                }
                return deferred.resolve(docs || []);
            });

        return deferred.promise;
    }

    getCount(filters) {
        var deferred = Q.defer();
        const { searchFilters } = getListFilters(filters, filterOpts);

        this._model.countDocuments(searchFilters).exec(function (err, docs) {
            if (err) {
                _console.error(err);
                return deferred.reject(new Error(err));
            }
            return deferred.resolve(docs);
        });

        return deferred.promise;
    }

    getById(id, user) {
        var deferred = Q.defer();
        this._model
            .findOne({ _id: id, user })
            .populate('user')
            .populate('parent_task')
            .populate('bucket')
            .exec(function (err, docs) {
                if (err) {
                    _console.log('Features.getById : ' + err.message);
                    return deferred.reject(new Error(err));
                }
                return deferred.resolve(docs || {});
            });

        return deferred.promise;
    }

    create(data) {
        const deferred = Q.defer();
        var newData = new this._model(data);
        newData.save((err, user) => {
            if (err) {
                _console.log('Failed to save user.');
                return deferred.reject(new Error(err));
            }
            return deferred.resolve(user);
        });

        return deferred.promise;
    }

    async update(data, user) {
        var deferred = Q.defer();
        this._model
            .findOneAndUpdate({ _id: data._id, user }, data, {
                new: true,
                upsert: true,
                overwrite: true,
            })
            .populate('user')
            .populate('parent_task')
            .populate('bucket')
            .exec((err, doc) => {
                if (err) {
                    return deferred.reject(new Error(err));
                }
                deferred.resolve(doc);
            });

        return deferred.promise;
    }

    delete({ id }, user) {
        var deferred = Q.defer();

        this._model.findOneAndUpdate(
            { _id: id, user: user },
            { $set: { status: 'DELETED' } },
            {
                new: true,
                upsert: true,
                overwrite: true,
            },
            (err, doc) => {
                if (err) {
                    return deferred.reject(new Error(err));
                }
                deferred.resolve(doc);
            }
        );

        return deferred.promise;
    }
}

export default new TasksModel();
