import * as mongoose from 'mongoose';
import * as bcrypt from 'bcryptjs';
import * as logger from 'tracer';
import { getListFilters } from '../../helpers/pageHelpers';

const _console = logger.colorConsole();

const Q = require('q');

const filterOpts = {
    like: ['title'],
    limit: 100,
};

class TasksModel {
    _schema;
    _model;
    constructor() {
        this._init();
    }
    _init() {
        this._defineSchema();
        this._initModel();
    }

    _defineSchema() {
        this._schema = new mongoose.Schema(
            {
                date: { type: Date, required: false },
                title: { type: String, required: false },
                index: { type: Number, required: false },
                isActive: { type: Boolean, required: false, default: false },
                slotId: { type: String, required: true },
                lastActive: { type: Date, required: false },
                time_logged: { type: Number, required: false }, // milliseconds
                status: { type: String, default: 'PENDING', required: false }, // COMPLETED, PENDING
                // Planning
                start_date: { type: Date, required: false },
                end_date: { type: Date, required: false },
                task: {
                    type: mongoose.Schema.Types.ObjectId,
                    required: false,
                    ref: 'tasks',
                },
                user: { type: mongoose.Schema.Types.ObjectId, required: true, ref: 'users' },
            },
            {
                timestamps: true,
                toObject: { virtuals: true },
                toJSON: { virtuals: true },
            }
        );
        this._schema.virtual('id').get(function (this: any) {
            return this._id;
        });
    }

    _initModel() {
        this._model = mongoose.model('board_tasks', this._schema);
    }

    getAll(filters) {
        var deferred = Q.defer();
        const { searchFilters, pageFilters } = getListFilters(filters, filterOpts);
        const self = this;
        if (searchFilters.bucket) {
            searchFilters['task.bucket'] = mongoose.Types.ObjectId(searchFilters.bucket);
            delete searchFilters.bucket;
            if (searchFilters.date.$gte) {
                searchFilters.date.$gte = new Date(searchFilters.date.$gte);
            }
            if (searchFilters.date.$lt) {
                searchFilters.date.$lt = new Date(searchFilters.date.$lt);
            }
            if (searchFilters.user) {
                searchFilters.user = mongoose.Types.ObjectId(searchFilters.user._id);
            }
            self._model
                .aggregate([
                    {
                        $lookup: {
                            from: 'tasks',
                            let: { taskId: '$task' },
                            pipeline: [{ $match: { $expr: { $in: ['$_id', ['$$taskId']] } } }],
                            // localField: 'task',
                            // foreignField: '_id',
                            as: 'task',
                        },
                    },
                    {
                        $set: { task: { $first: '$task' } },
                    },
                    {
                        $set: { id: '$_id' },
                    },
                    {
                        $match: {
                            ...searchFilters,
                        },
                    },
                    {
                        $lookup: {
                            from: 'tasks_bucket',
                            localField: 'task.bucket',
                            foreignField: '_id',
                            as: 'bucket',
                        },
                    },
                    {
                        $project: {
                            _id: 1,
                        },
                    },
                    { $limit: pageFilters.limit },
                    { $skip: pageFilters.offset },
                    { $sort: pageFilters.sort },
                ])
                .exec(function (err, docs) {
                    if (err) {
                        _console.log('Task Focus Boards.getAll : ' + err.message);
                        return deferred.reject(new Error(err));
                    }
                    
                    self._model
                        .find({ _id: { $in: docs.map((_doc) => _doc._id) } })
                        .populate('user')
                        .populate('task')
                        .populate({
                            path: 'task',
                            populate: {
                                path: 'bucket',
                            },
                        })
                        .limit(pageFilters.limit)
                        .skip(pageFilters.offset)
                        .sort(pageFilters.sort)
                        .exec(function (err, docs) {
                            if (err) {
                                _console.log('Task Focus Boards.getAll : ' + err.message);
                                return deferred.reject(new Error(err));
                            }
                            return deferred.resolve(docs || []);
                        });
                });
        } else {
            // if (searchFilters.bucket) {
            //     searchFilters['task.bucket'] = mongoose.Types.ObjectId(searchFilters.bucket);
            //     delete searchFilters.bucket;
            // }
            this._model
                .find(searchFilters)
                .populate('user')
                .populate('task')
                .populate({
                    path: 'task',
                    populate: {
                        path: 'bucket',
                    },
                })
                .limit(pageFilters.limit)
                .skip(pageFilters.offset)
                .sort(pageFilters.sort)
                .exec(function (err, docs) {
                    if (err) {
                        _console.log('Task Focus Boards.getAll : ' + err.message);
                        return deferred.reject(new Error(err));
                    }
                    return deferred.resolve(docs || []);
                });
        }
        return deferred.promise;
    }

    getCount(filters) {
        var deferred = Q.defer();
        const { searchFilters } = getListFilters(filters, filterOpts);

        this._model.countDocuments(searchFilters).exec(function (err, docs) {
            if (err) {
                _console.error(err);
                return deferred.reject(new Error(err));
            }
            return deferred.resolve(docs);
        });

        return deferred.promise;
    }

    getById(id, user) {
        var deferred = Q.defer();
        this._model
            .findOne({ _id: id, user })
            .populate('user')
            .populate('task')
            .populate({
                path: 'task',
                populate: {
                    path: 'bucket',
                },
            })
            .exec(function (err, docs) {
                if (err) {
                    _console.log('Task Focus Boards.getById : ' + err.message);
                    return deferred.reject(new Error(err));
                }
                return deferred.resolve(docs || {});
            });

        return deferred.promise;
    }

    create(data) {
        const deferred = Q.defer();
        var newData = new this._model(data);
        newData.save((err, user) => {
            if (err) {
                _console.log('Failed to save task.');
                return deferred.reject(new Error(err));
            }
            return deferred.resolve(user);
        });

        return deferred.promise;
    }

    async update(data, user) {
        var deferred = Q.defer();
        this._model
            .findOneAndUpdate({ _id: data._id, user }, data, {
                new: true,
                upsert: true,
                overwrite: true,
            })
            .populate('user')
            .populate('task')
            .populate({
                path: 'task',
                populate: {
                    path: 'bucket',
                },
            })
            .exec((err, doc) => {
                if (err) {
                    return deferred.reject(new Error(err));
                }
                deferred.resolve(doc);
            });

        return deferred.promise;
    }

    delete({ id }, user) {
        var deferred = Q.defer();

        this._model.findOneAndUpdate(
            { _id: id, user: user.id },
            { $set: { status: 'DELETED' } },
            {
                new: true,
                upsert: true,
                overwrite: true,
            },
            (err, doc) => {
                if (err) {
                    return deferred.reject(new Error(err));
                }
                deferred.resolve(doc);
            }
        );

        return deferred.promise;
    }
}

export default new TasksModel();
