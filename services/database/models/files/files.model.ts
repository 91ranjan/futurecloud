import * as mongoose from 'mongoose';
import * as bcrypt from 'bcryptjs';
import * as logger from 'tracer';
import { getListFilters } from '../../helpers/pageHelpers';
import { getMongoFilters } from '../../helpers/filters';

const _console = logger.colorConsole();

const Q = require('q');

const filterOpts = {
    like: ['name'],
};

const generateHash = function (password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

class Model {
    _schema;
    _model;
    constructor() {
        this._init();
    }
    _init() {
        this._defineSchema();
        this._initModel();
    }

    _defineSchema() {
        this._schema = new mongoose.Schema(
            {
                name: { type: String, required: true },
                path: { type: String, required: true }, // the file system path
                dir: { type: String, required: true }, // the directory path inside the user directory
                kind: { type: String, required: true }, // Type could be file, symlink, folder
                link: { type: String, required: false }, // Direct link to the file
                download_link: { type: String, required: false }, // Download link to the file
                isLocal: { type: Boolean, required: true }, // is file saved in the local file system
                favorite: { type: Boolean, required: false },
                size: { type: String, required: false, default: 0 },
                type: { type: String, required: true, default: 'unknown' },
                parent: {
                    type: mongoose.Schema.Types.ObjectId,
                    required: false,
                    ref: 'files_files',
                },
                thirdPartyData: { type: mongoose.Schema.Types.Mixed, required: false },
                metadata: {
                    ext: { type: String, required: false },
                    mime: { type: String, required: false },
                    audio: {
                        title: { type: String, required: false },
                        artists: [{ type: String, required: false }],
                        duration: { type: Number, required: false },
                        bitrate: { type: Number, required: false },
                        artist: { type: String, required: false },
                        album: { type: String, required: false },
                        year: { type: String, required: false },
                        genre: [{ type: String, required: false }],
                        comment: [{ type: String, required: false }],
                        componser: [{ type: String, required: false }],
                        picture: [
                            {
                                format: { type: String, required: false },
                                type: { type: String, required: false },
                                description: { type: String, required: false },
                                url: { type: String, required: false },
                            },
                        ],
                    },
                },
                user: { type: mongoose.Schema.Types.ObjectId, required: true, ref: 'users' },
                storage: {
                    type: mongoose.Schema.Types.ObjectId,
                    required: false,
                    ref: 'settings_storage',
                },
                shared_with: [
                    { type: mongoose.Schema.Types.ObjectId, required: true, ref: 'users' },
                ],
            },
            {
                timestamps: true,
                toObject: { virtuals: true },
                toJSON: { virtuals: true },
            }
        );
        this._schema.virtual('id').get(function (this: any) {
            return this._id;
        });
    }

    _initModel() {
        this._model = mongoose.model('files_files', this._schema);
    }

    getAll(filters) {
        var deferred = Q.defer();
        let { searchFilters, pageFilters } = getListFilters(filters, filterOpts);
        searchFilters = getMongoFilters(searchFilters);

        this._model
            .find(searchFilters)
            .populate('user')
            .populate('parent')
            .populate('shared_with')
            .populate('storage')
            .limit(pageFilters.limit)
            .skip(pageFilters.offset)
            .sort({ createdAt: -1 })
            .exec(function (err, docs) {
                if (err) {
                    _console.log('Features.getAll : ' + err.message);
                    return deferred.reject(new Error(err));
                }
                return deferred.resolve(docs || []);
            });

        return deferred.promise;
    }

    getCount(filters) {
        var deferred = Q.defer();
        let { searchFilters } = getListFilters(filters, filterOpts);
        searchFilters = getMongoFilters(searchFilters);
        this._model.countDocuments(searchFilters).exec(function (err, docs) {
            if (err) {
                _console.error(err);
                return deferred.reject(new Error(err));
            }
            return deferred.resolve(docs);
        });

        return deferred.promise;
    }

    getById(id) {
        var deferred = Q.defer();
        this._model
            .findOne({ _id: id })
            .populate('user')
            .populate('parent')
            .populate('shared_with')
            .populate('storage')
            .exec(function (err, docs) {
                if (err) {
                    _console.log('Features.getById : ' + err.message);
                    return deferred.reject(new Error(err));
                }
                return deferred.resolve(docs);
            });

        return deferred.promise;
    }

    getByRootFolder(filter) {
        var deferred = Q.defer();
        this._model
            .findOne(filter)
            .populate('user')
            .populate('parent')
            .populate('shared_with')
            .populate('storage')
            .exec(function (err, docs) {
                if (err) {
                    _console.log('Features.getById : ' + err.message);
                    return deferred.reject(new Error(err));
                }
                return deferred.resolve(docs);
            });

        return deferred.promise;
    }

    create(data) {
        const deferred = Q.defer();
        var newData = new this._model(data);
        newData.save((err, file) => {
            if (err) {
                _console.log('Failed to save file.');
                return deferred.reject(new Error(err));
            }
            return deferred.resolve(file);
        });

        return deferred.promise;
    }

    async update(data) {
        var deferred = Q.defer();
        this._model
            .findOneAndUpdate({ _id: data._id, user: data.user.id }, data, {
                new: true,
                upsert: true,
                overwrite: true,
            })
            .populate('user')
            .populate('parent')
            .populate('shared_with')
            .populate('storage')
            .exec((err, doc) => {
                if (err) {
                    return deferred.reject(new Error(err));
                }
                deferred.resolve(doc);
            });

        return deferred.promise;
    }

    delete({ id, userId }) {
        var deferred = Q.defer();

        this._model.findOneAndRemove({ _id: id, user: userId }, (err, doc) => {
            if (err) {
                return deferred.reject(new Error(err));
            }
            deferred.resolve(doc);
        });

        return deferred.promise;
    }
}

export default new Model();
