import events from './events.model';
import calendar from './calendar.model';

export default {
    events,
    calendar,
};
