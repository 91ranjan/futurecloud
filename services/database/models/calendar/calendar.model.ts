import * as mongoose from 'mongoose';
import * as bcrypt from 'bcryptjs';
import * as logger from 'tracer';
import { getListFilters } from '../../helpers/pageHelpers';

const _console = logger.colorConsole();

const Q = require('q');

const filterOpts = {
    like: ['name'],
};

const generateHash = function (password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

class CalendarsModel {
    _schema;
    _model;
    constructor() {
        this._init();
    }
    _init() {
        this._defineSchema();
        this._initModel();
    }

    _defineSchema() {
        this._schema = new mongoose.Schema(
            {
                name: { type: String, required: true },
                color: { type: String, required: false },
                private_link: { type: String, required: false },
                user: { type: mongoose.Schema.Types.ObjectId, required: true, ref: 'users' },
            },
            {
                timestamps: true,
                toObject: { virtuals: true },
                toJSON: { virtuals: true },
            }
        );
        this._schema.virtual('id').get(function (this: any) {
            return this._id;
        });
    }

    _initModel() {
        this._model = mongoose.model('calendar_calendars', this._schema);
    }

    getAll(filters) {
        var deferred = Q.defer();
        const { searchFilters, pageFilters } = getListFilters(filters, filterOpts);
        this._model
            .find(searchFilters)
            .populate('user')
            .limit(pageFilters.limit)
            .skip(pageFilters.offset)
            .sort({ createdAt: -1 })
            .exec(function (err, docs) {
                if (err) {
                    _console.log('Features.getAll : ' + err.message);
                    return deferred.reject(new Error(err));
                }
                return deferred.resolve(docs || []);
            });

        return deferred.promise;
    }

    getCount(filters) {
        var deferred = Q.defer();
        const { searchFilters } = getListFilters(filters, filterOpts);

        this._model.countDocuments(searchFilters).exec(function (err, docs) {
            if (err) {
                _console.error(err);
                return deferred.reject(new Error(err));
            }
            return deferred.resolve(docs);
        });

        return deferred.promise;
    }

    getById(id) {
        var deferred = Q.defer();
        this._model
            .findOne({ _id: id })
            .populate('user')
            .exec(function (err, docs) {
                if (err) {
                    _console.log('Features.getById : ' + err.message);
                    return deferred.reject(new Error(err));
                }
                return deferred.resolve(docs || {});
            });

        return deferred.promise;
    }

    getByEmail(email) {
        var deferred = Q.defer();
        this._model.findOne({ email }).exec(function (err, docs) {
            if (err) {
                _console.log('Features.getById : ' + err.message);
                return deferred.reject(new Error(err));
            }
            return deferred.resolve(docs);
        });

        return deferred.promise;
    }

    create(data) {
        const deferred = Q.defer();
        var newData = new this._model(data);
        newData.save((err, user) => {
            if (err) {
                _console.log('Failed to save user.');
                return deferred.reject(new Error(err));
            }
            return deferred.resolve(user);
        });

        return deferred.promise;
    }

    async update(data) {
        var deferred = Q.defer();
        this._model
            .findOneAndUpdate({ _id: data._id, user: data.user.id }, data, {
                new: true,
                upsert: true,
                overwrite: true,
            })
            .populate('user')
            .exec((err, doc) => {
                if (err) {
                    return deferred.reject(new Error(err));
                }
                deferred.resolve(doc);
            });

        return deferred.promise;
    }

    resetPassword(id, email, password) {
        var deferred = Q.defer();
        let local_set = {};

        local_set['local.password'] = generateHash(password);

        this._model.findOneAndUpdate(
            { _id: id, 'local.email': email },
            { $set: local_set },
            { new: true, upsert: true },
            (err, data) => {
                if (err) {
                    _console.log('Failed to update user.');
                    return deferred.reject(new Error(err));
                }
                return deferred.resolve(data);
            }
        );
        return deferred.promise;
    }

    delete({ id, user }) {
        var deferred = Q.defer();

        this._model.findOneAndRemove({ _id: id, user: user.id }, (err, doc) => {
            if (err) {
                return deferred.reject(new Error(err));
            }
            deferred.resolve(doc);
        });

        return deferred.promise;
    }
}

export default new CalendarsModel();
