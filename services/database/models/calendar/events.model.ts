import * as mongoose from 'mongoose';
import * as bcrypt from 'bcryptjs';
import * as logger from 'tracer';
import { getListFilters } from '../../helpers/pageHelpers';

const _console = logger.colorConsole();

const Q = require('q');

const filterOpts = {
    like: ['local.email'],
};

function createDateRangeFilter(filters) {
    const $or = filters.$or || [];
    if (filters.from && filters.to) {
        $or.push({
            $and: [
                {
                    from: { $gte: new Date(filters.from) },
                },
                {
                    from: { $lte: new Date(filters.to) },
                },
            ],
        });
        $or.push({
            $and: [
                {
                    to: { $gte: new Date(filters.from) },
                },
                {
                    to: { $lte: new Date(filters.to) },
                },
            ],
        });
        filters.to && delete filters.to;
        filters.from && delete filters.from;
    }
    if ($or.length) {
        filters.$or = $or;
    }
    return filters;
}
class EventsModel {
    _schema;
    _model;
    constructor() {
        this._init();
    }
    _init() {
        this._defineSchema();
        this._initModel();
    }

    _defineSchema() {
        this._schema = new mongoose.Schema(
            {
                organizer: { type: mongoose.Schema.Types.ObjectId, required: true, ref: 'users' },
                calendar: {
                    type: mongoose.Schema.Types.ObjectId,
                    required: true,
                    ref: 'calendar_calendars',
                },
                title: { type: String, required: true },
                from: { type: Date, required: true },
                to: { type: Date, required: true },
                all_day: { type: Boolean, required: false },
                details: {
                    kind: { type: String, required: false },
                    location: { type: String, required: false },
                    description: { type: String, required: false },
                    confirmation: { type: String, required: true },
                    user_status: { type: String, required: false },
                    color: { type: String, required: false },
                },
                attendees: [{ type: mongoose.Schema.Types.ObjectId, required: true, ref: 'users' }],
                reminders: [{ type: String, required: false }],
            },
            {
                timestamps: true,
                toObject: { virtuals: true },
                toJSON: { virtuals: true },
            }
        );
        this._schema.virtual('id').get(function (this: any) {
            return this._id;
        });
    }

    _initModel() {
        this._model = mongoose.model('calendar_events', this._schema);
    }

    getAll(filters) {
        var deferred = Q.defer();
        const { searchFilters, pageFilters } = getListFilters(filters, filterOpts);
        createDateRangeFilter(searchFilters);

        // _console.log(JSON.stringify(searchFilters, null, 4));
        this._model
            .find(searchFilters)
            .populate('organizer')
            .populate('attendees')
            .populate('calendar')
            .limit(pageFilters.limit)
            .skip(pageFilters.offset)
            .sort({ createdAt: -1 })
            .exec(function (err, docs) {
                if (err) {
                    _console.log('Features.getAll : ' + err.message);
                    return deferred.reject(new Error(err));
                }
                return deferred.resolve(docs || []);
            });

        return deferred.promise;
    }

    getCount(filters) {
        var deferred = Q.defer();
        const { searchFilters } = getListFilters(filters, filterOpts);
        createDateRangeFilter(searchFilters);

        this._model.countDocuments(searchFilters).exec(function (err, docs) {
            if (err) {
                _console.error(err);
                return deferred.reject(new Error(err));
            }
            return deferred.resolve(docs);
        });

        return deferred.promise;
    }

    getById(id) {
        var deferred = Q.defer();
        this._model
            .findOne({ _id: id })
            .populate('organizer')
            .populate('attendees')
            .populate('calendar')
            .exec(function (err, docs) {
                if (err) {
                    _console.log('Features.getById : ' + err.message);
                    return deferred.reject(new Error(err));
                }
                return deferred.resolve(docs || {});
            });

        return deferred.promise;
    }

    getByEmail(email) {
        var deferred = Q.defer();
        this._model.findOne({ email }).exec(function (err, docs) {
            if (err) {
                _console.log('Features.getById : ' + err.message);
                return deferred.reject(new Error(err));
            }
            return deferred.resolve(docs);
        });

        return deferred.promise;
    }

    create(data) {
        const deferred = Q.defer();
        var newUser = new this._model(data);

        newUser.save((err, user) => {
            if (err) {
                _console.log('Failed to save user.');
                return deferred.reject(new Error(err));
            }
            return deferred.resolve(user);
        });

        return deferred.promise;
    }

    async update(data) {
        var deferred = Q.defer();
        this._model
            .findOneAndUpdate({ _id: data._id }, data, { new: true, upsert: true })
            .populate('organizer')
            .populate('attendees')
            .populate('calendar')
            .exec((err, doc) => {
                if (err) {
                    return deferred.reject(new Error(err));
                }
                deferred.resolve(doc);
            });

        return deferred.promise;
    }

    delete({ id, organizer }) {
        var deferred = Q.defer();
        _console.log({ id, organizer });
        this._model.findOneAndRemove({ _id: id, organizer: organizer }, (err, doc) => {
            if (err) {
                return deferred.reject(new Error(err));
            }
            deferred.resolve(doc);
        });

        return deferred.promise;
    }
}

export default new EventsModel();
