import * as mongoose from 'mongoose';
import * as bcrypt from 'bcryptjs';
import * as logger from 'tracer';
import { getListFilters } from '../../helpers/pageHelpers';
import { getMongoFilters } from '../../helpers/filters';

const _console = logger.colorConsole();

const Q = require('q');

const filterOpts = {
    like: ['title'],
};

const generateHash = function (password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

class MusicModel {
    _schema;
    _model;
    constructor() {
        this._init();
    }
    _init() {
        this._defineSchema();
        this._initModel();
    }

    _defineSchema() {
        this._schema = new mongoose.Schema(
            {
                title: { type: String, required: true },
                album: { type: String, required: false },
                duration: { type: Number, required: false },
                favourite: { type: Boolean, required: false, default: false },
                genres: [{ type: String, required: false }],
                artists: [{ type: String, required: false }],
                composers: [{ type: String, required: false }],
                playlist: [
                    {
                        type: mongoose.Schema.Types.ObjectId,
                        required: false,
                        ref: 'music_playlist',
                    },
                ],
                folder: {
                    type: mongoose.Schema.Types.ObjectId,
                    required: true,
                    ref: 'files_files',
                },
                user: { type: mongoose.Schema.Types.ObjectId, required: true, ref: 'users' },
                file: { type: mongoose.Schema.Types.ObjectId, required: true, ref: 'files_files' },
            },
            {
                timestamps: true,
                toObject: { virtuals: true },
                toJSON: { virtuals: true },
            }
        );
        this._schema.virtual('id').get(function (this: any) {
            return this._id;
        });
    }

    _initModel() {
        this._model = mongoose.model('music_library', this._schema);
    }

    getAll(filters) {
        var deferred = Q.defer();
        let { searchFilters, pageFilters } = getListFilters(filters, filterOpts);
        searchFilters = getMongoFilters(searchFilters);

        this._model
            .find(searchFilters)
            .populate('user')
            .populate('file')
            .limit(pageFilters.limit)
            .skip(pageFilters.offset)
            .sort({ createdAt: -1 })
            .exec(function (err, docs) {
                if (err) {
                    _console.log('Features.getAll : ' + err.message);
                    return deferred.reject(new Error(err));
                }
                return deferred.resolve(docs || []);
            });

        return deferred.promise;
    }

    getCount(filters) {
        var deferred = Q.defer();
        let { searchFilters } = getListFilters(filters, filterOpts);
        searchFilters = getMongoFilters(searchFilters);
        this._model.countDocuments(searchFilters).exec(function (err, docs) {
            if (err) {
                _console.error(err);
                return deferred.reject(new Error(err));
            }
            return deferred.resolve(docs);
        });

        return deferred.promise;
    }

    getById(id, userId) {
        var deferred = Q.defer();
        this._model
            .findOne({ _id: id, user: userId })
            .populate('user')
            .populate('file')
            .exec(function (err, docs) {
                if (err) {
                    _console.log('Features.getById : ' + err.message);
                    return deferred.reject(new Error(err));
                }
                return deferred.resolve(docs);
            });

        return deferred.promise;
    }

    getOne(filter) {
        var deferred = Q.defer();
        this._model
            .findOne(filter)
            .populate('user')
            .populate('file')
            .exec(function (err, docs) {
                if (err) {
                    _console.log('Features.getById : ' + err.message);
                    return deferred.reject(new Error(err));
                }
                return deferred.resolve(docs);
            });

        return deferred.promise;
    }

    create(data) {
        const deferred = Q.defer();
        var newData = new this._model(data);
        newData.save((err, user) => {
            if (err) {
                _console.log('Failed to save user.');
                return deferred.reject(new Error(err));
            }
            return deferred.resolve(user);
        });

        return deferred.promise;
    }

    async update(data) {
        var deferred = Q.defer();
        this._model
            .findOneAndUpdate({ _id: data._id, user: data.user.id }, data, {
                new: true,
                upsert: true,
                overwrite: true,
            })
            .populate('user')
            .populate('file')
            .exec((err, doc) => {
                if (err) {
                    return deferred.reject(new Error(err));
                }
                deferred.resolve(doc);
            });

        return deferred.promise;
    }

    delete({ id, userId }) {
        var deferred = Q.defer();

        this._model.findOneAndRemove({ _id: id, user: userId }, (err, doc) => {
            if (err) {
                return deferred.reject(new Error(err));
            }
            deferred.resolve(doc);
        });

        return deferred.promise;
    }
}

export default new MusicModel();
