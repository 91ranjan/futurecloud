import music from './music.model';
import playlist from './music_playlist.model';

export default {
    music,
    playlist,
};
