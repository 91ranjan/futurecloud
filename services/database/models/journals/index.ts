import books from './journal_book.model';
import entries from './journal_entry.model';

export default {
    books,
    entries,
};
