import storage from './storage.model';
import module from './module.model';

export default {
    storage,
    module
};
