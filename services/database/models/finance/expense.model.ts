import * as mongoose from 'mongoose';
import * as bcrypt from 'bcryptjs';
import * as logger from 'tracer';
import { getListFilters } from '../../helpers/pageHelpers';
import { getMongoFilters } from '../../helpers/filters';

const _console = logger.colorConsole();

const Q = require('q');

const filterOpts = {
    like: ['item'],
    limit: 100,
};

class MusicModel {
    _schema;
    _model;
    constructor() {
        this._init();
    }
    _init() {
        this._defineSchema();
        this._initModel();
    }

    _defineSchema() {
        this._schema = new mongoose.Schema(
            {
                item: { type: String, required: true },
                description: { type: String, required: false },
                expected_amount: { type: Number, required: false, default: 0 },
                amount: { type: Number, required: false, default: 0 },
                date: { type: String, required: true },
                slotId: { type: String, required: true },
                mode: { type: String, required: true, default: 'online' }, // online
                transaction_type: { type: String, required: true, default: 'debit' }, // debit
                status: { type: String, required: true, default: 'paid' }, // paid
                merchant: { type: String, required: false },
                category: { type: String, required: false },
                paidBy: { type: String, required: false },
                user: { type: mongoose.Schema.Types.ObjectId, required: true, ref: 'users' },
                file: { type: mongoose.Schema.Types.ObjectId, required: false, ref: 'files_files' },
            },
            {
                timestamps: true,
                toObject: { virtuals: true },
                toJSON: { virtuals: true },
            }
        );
        this._schema.virtual('id').get(function (this: any) {
            return this._id;
        });
    }

    _initModel() {
        this._model = mongoose.model('finance_expense', this._schema);
    }

    getAll(filters) {
        var deferred = Q.defer();
        let { searchFilters, pageFilters } = getListFilters(filters, filterOpts);
        searchFilters = getMongoFilters(searchFilters);

        this._model
            .find(searchFilters)
            .populate('user')
            .limit(pageFilters.limit)
            .skip(pageFilters.offset)
            .sort({ date: 1 })
            .exec(function (err, docs) {
                if (err) {
                    _console.log('Expense.getAll : ' + err.message);
                    return deferred.reject(new Error(err));
                }
                return deferred.resolve(docs || []);
            });

        return deferred.promise;
    }

    getCount(filters) {
        var deferred = Q.defer();
        let { searchFilters } = getListFilters(filters, filterOpts);
        searchFilters = getMongoFilters(searchFilters);
        this._model.countDocuments(searchFilters).exec(function (err, docs) {
            if (err) {
                _console.error(err);
                return deferred.reject(new Error(err));
            }
            return deferred.resolve(docs);
        });

        return deferred.promise;
    }

    getById(id, user) {
        var deferred = Q.defer();
        this._model
            .findOne({ _id: id, user })
            .populate('user')
            .exec(function (err, docs) {
                if (err) {
                    _console.log('Expense.getById : ' + err.message);
                    return deferred.reject(new Error(err));
                }
                return deferred.resolve(docs);
            });

        return deferred.promise;
    }

    getOne(filter) {
        var deferred = Q.defer();
        this._model
            .findOne(filter)
            .populate('user')
            .exec(function (err, docs) {
                if (err) {
                    _console.log('Expense.getById : ' + err.message);
                    return deferred.reject(new Error(err));
                }
                return deferred.resolve(docs);
            });

        return deferred.promise;
    }

    create(data) {
        const deferred = Q.defer();
        var newData = new this._model(data);
        newData.save((err, user) => {
            if (err) {
                _console.log('Failed to save user.');
                return deferred.reject(new Error(err));
            }
            return deferred.resolve(user);
        });

        return deferred.promise;
    }

    async update(data, user) {
        var deferred = Q.defer();
        this._model
            .findOneAndUpdate({ _id: data._id, user }, data, {
                new: true,
                upsert: true,
                overwrite: true,
            })
            .populate('user')
            .exec((err, doc) => {
                if (err) {
                    return deferred.reject(new Error(err));
                }
                deferred.resolve(doc);
            });

        return deferred.promise;
    }

    delete({ id }, user) {
        var deferred = Q.defer();

        this._model.findOneAndRemove({ _id: id, user }, (err, doc) => {
            if (err) {
                return deferred.reject(new Error(err));
            }
            deferred.resolve(doc);
        });

        return deferred.promise;
    }
}

export default new MusicModel();
