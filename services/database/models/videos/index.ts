import video from './video.model';
import playlist from './video_playlist.model';

export default {
    video,
    playlist,
};
