import trips from './trip.model';
import checklist from './trip_checklist.model';
import expenses from './trip_expense.model';
import commute from './trip_commute.model';
import lodge from './trip_lodge.model';
import places from './trip_places.model';
import activity from './trip_activity.model';
import notes from './trip_notes.model';

export default {
    trips,
    checklist,
    expenses,
    commute,
    lodge,
    places,
    activity,
    notes
};
