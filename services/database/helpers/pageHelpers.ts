export const getListFilters = ({ limit, page = 1, sort = { createdAt: -1 }, ...filters }, opts) => {
    opts = opts || {};
    const pagelimit = limit ? parseInt(limit, 10) : opts.limit || 20;
    if (opts.like) {
        Object.keys(filters).forEach((key) => {
            if (opts.like.indexOf(key) > -1) {
                filters[key] = { $regex: filters[key], $options: 'i' };
            }
        });
    }

    return {
        pageFilters: {
            limit: pagelimit,
            offset: (page - 1) * pagelimit,
            sort,
        },
        searchFilters: filters,
    };
};
