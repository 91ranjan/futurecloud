export function getMongoFilters(filters, formatter?) {
    const mongoFilters = filters;
    formatter =
        formatter ||
        function (key, value) {
            value;
        };
    for (let key in filters) {
        if (typeof filters[key] === 'object') {
            if (filters[key].eq) {
                mongoFilters[key] = {
                    $eq: formatter(key, filters[key].eq),
                };
            } else if (filters[key].lt || filters[key].lte || filters[key].gt || filters[key].gte) {
                if (filters[key].lt) {
                    filters[key].$lt = formatter(key, filters[key].lt);
                    delete filters[key].lt;
                }
                if (filters[key].lte) {
                    filters[key].$lte = formatter(key, filters[key].lte);
                    delete filters[key].lte;
                }
                if (filters[key].gt) {
                    filters[key].$gt = formatter(key, filters[key].gt);
                    delete filters[key].gt;
                }
                if (filters[key].gte) {
                    filters[key].$gte = formatter(key, filters[key].gte);
                    delete filters[key].gte;
                }
            } else if (filters[key].hasOwnProperty('exists')) {
                filters[key].$exists = filters[key].exists;
                delete filters[key].exists;
            }
        }
    }
    return mongoFilters;
}
