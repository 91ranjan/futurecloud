import videoCtrl from './controllers/video.ctrl';
import videoPlaylistCtrl from './controllers/video_playlist.ctrl';
import * as videoConstants from './constants/video.contant';

export default {
    controllers: { list: videoCtrl, playlist: videoPlaylistCtrl },
    constants: videoConstants,
};
