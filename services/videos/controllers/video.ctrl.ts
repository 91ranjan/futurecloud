import * as logger from 'tracer';
import { asyncForEach } from '../../../common/utils/asyncHelpers';
const _console = logger.colorConsole();

import services from '../../index';

export async function getAll(filters) {
    try {
        return await services.database.videos.video.getAll(filters);
    } catch (e) {
        throw e;
    }
}

export async function count(filters) {
    try {
        return await services.database.videos.video.getCount(filters);
    } catch (e) {
        throw e;
    }
}

export async function getById(id) {
    try {
        return await services.database.videos.video.getById(id);
    } catch (e) {
        throw e;
    }
}

export async function getByFileId(id) {
    try {
        return await services.database.videos.video.getOne({ file: id });
    } catch (e) {
        throw e;
    }
}

export async function create(event) {
    try {
        return await services.database.videos.video.create(event);
    } catch (e) {
        throw e;
    }
}

export async function update(event) {
    try {
        return await services.database.videos.video.update(event);
    } catch (e) {}
}

export async function del(request) {
    const { id, userId } = request;
    try {
        return await services.database.videos.video.delete({ id, userId });
    } catch (e) {
        throw e;
    }
}

async function addVideoFromFile(file) {
    const videoFile = await services.video.controllers.list.getByFileId(file.id);
    let rootFolder = await services.files.getByRootFolder({
        user: file.user.id,
        kind: 'FOLDER',
        name: file.user.local.username,
    });
    if (!videoFile) {
        // Create one
        const data = {
            title: file.name,
            album: file?.metadata?.audio?.album,
            genres: file?.metadata?.audio?.genre,
            artists: file?.metadata?.audio?.artists,
            composers: file?.metadata?.audio?.composers || [],
            folder: file.parent ? file.parent.id : rootFolder.id,
            user: file.user.id,
            file: file.id,
        };
        _console.debug(file.parent);
        _console.debug(rootFolder.id);
        await services.video.controllers.list.create(data);
    }
}

// Start the music scanner
async function scanVideo() {
    const videoFiles = await services.database.files.files.getAll({
        'metadata.ext': { $in: services.video.constants.supportedext },
        'metadata.mime': { $in: services.video.constants.supportedMime },
        page: 1,
        limit: 1000,
    });

    await asyncForEach(videoFiles, async (file) => {
        await addVideoFromFile(file);
        // Also check stale music files to delete them
    });
}

export default {
    getAll,
    count,
    getById,
    create,
    update,
    del,
    getByFileId,
    scanVideo,
    addVideoFromFile,
};
