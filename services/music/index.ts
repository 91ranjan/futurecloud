import musicCtrl from './controllers/music.ctrl';
import musicPlaylistCtrl from './controllers/music_playlist.ctrl';
import * as musicConstants from './constants/music.contant';
import musicEvents from './constants/music_events.constant';

export default {
    controllers: { list: musicCtrl, playlist: musicPlaylistCtrl },
    constants: { ...musicConstants, events: musicEvents },
};
