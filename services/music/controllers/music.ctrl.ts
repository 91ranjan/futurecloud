import * as logger from 'tracer';
import * as mongoose from 'mongoose';
import { asyncForEach } from '../../../common/utils/asyncHelpers';
const _console = logger.colorConsole();

import services from '../../index';
import cloudFileManager from '../../files/utils/cloudFileManager';

export async function getAll(filters) {
    try {
        return await services.database.music.music.getAll(filters);
    } catch (e) {
        throw e;
    }
}

export async function count(filters) {
    try {
        return await services.database.music.music.getCount(filters);
    } catch (e) {
        throw e;
    }
}

export async function getById(id, userId) {
    try {
        return await services.database.music.music.getById(id, userId);
    } catch (e) {
        throw e;
    }
}

export async function getByFileId(id) {
    try {
        return await services.database.music.music.getOne({ file: id });
    } catch (e) {
        throw e;
    }
}

export async function create(event) {
    try {
        return await services.database.music.music.create(event);
    } catch (e) {
        throw e;
    }
}

export async function update(event) {
    try {
        return await services.database.music.music.update(event);
    } catch (e) {}
}

export async function del(id, userId) {
    try {
        return await services.database.music.music.delete({ id, userId });
    } catch (e) {
        throw e;
    }
}

async function addMusicFromFile(file) {
    const musicFile = await services.music.controllers.list.getByFileId(file.id);
    let rootFolder = await services.files.getByRootFolder({
        user: file.user.id,
        kind: 'FOLDER',
        name: file.user.local.username,
    });
    if (!musicFile) {
        // Create one
        await services.music.controllers.list.create({
            title: file.name,
            album: file.metadata.audio.album,
            genres: file.metadata.audio.genre,
            artists: file.metadata.audio.artists,
            composers: file.metadata.audio.composers,
            duration: file.metadata.audio.duration,
            folder: file.parent ? file.parent.id : rootFolder.id,
            user: file.user.id,
            file: file.id,
            playlist: [],
        });
    }
}

async function getDownloadLink(musicId, user) {
    const musicFile = await services.music.controllers.list.getById(musicId, user.id);
    return await services.files.getDownloadLink(musicFile.file.id, user);
}

async function getDownloadRequestLink(musicId, user) {
    const musicFile = await services.music.controllers.list.getById(musicId, user.id);
    const storage = await services.settings.controllers.storage.getAll({
        user: user.id,
        isActive: true,
    });
    return cloudFileManager.downloadRequestLink(musicFile.file.thirdPartyData, storage[0]);
}

/**
 * Services is not defined when the file loads, so we use setTimeout
 */
setTimeout(() => {
    services.events.listenToEvent(
        services.files.constants.event_types.FILE_UPLOAD_SUCCESS,
        async (file) => {
            _console.debug('Recieved a file upload event.');
            if (
                services.music.constants.supportedext.includes(file.metadata.ext) &&
                services.music.constants.supportedMime.includes(file.metadata.mime)
            ) {
                await addMusicFromFile(file);
            }
        }
    );
    services.events.listenToEvent(
        services.files.constants.event_types.FILE_DELETE_SUCCESS,
        async (file) => {
            if (
                services.music.constants.supportedext.includes(file.metadata.ext) &&
                services.music.constants.supportedMime.includes(file.metadata.mime)
            ) {
                await services.database.music.music._model.findOneAndRemove({
                    file: mongoose.Types.ObjectId(file.id),
                });
            }
        }
    );
}, 10);

// Start the music scanner
async function scanMusic() {
    const musicFiles = await services.database.files.files.getAll({
        'metadata.ext': { $in: services.music.constants.supportedext },
        'metadata.mime': { $in: services.music.constants.supportedMime },
        page: 1,
        limit: 1000,
    });

    await asyncForEach(musicFiles, async (file) => {
        await addMusicFromFile(file);
        // Also check stale music files to delete them
    });
}

export default {
    getAll,
    count,
    getById,
    create,
    update,
    del,
    getByFileId,
    scanMusic,
    addMusicFromFile,
    getDownloadRequestLink,
    getDownloadLink,
};
