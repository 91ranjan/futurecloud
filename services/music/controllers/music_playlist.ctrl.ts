import * as logger from 'tracer';
const _console = logger.colorConsole();

import services from '../../index';

export async function getAll(filters) {
    try {
        return await services.database.music.playlist.getAll(filters);
    } catch (e) {
        throw e;
    }
}

export async function count(filters) {
    try {
        return await services.database.music.playlist.getCount(filters);
    } catch (e) {
        throw e;
    }
}

export async function getById(id) {
    try {
        return await services.database.music.playlist.getById(id);
    } catch (e) {
        throw e;
    }
}

export async function create(event) {
    try {
        return await services.database.music.playlist.create(event);
    } catch (e) {
        throw e;
    }
}

export async function update(event) {
    try {
        return await services.database.music.playlist.update(event);
    } catch (e) {}
}

export async function del(request) {
    const { id, userId } = request;
    try {
        return await services.database.music.playlist.delete({ id, userId });
    } catch (e) {
        throw e;
    }
}

export default {
    getAll,
    count,
    getById,
    create,
    update,
    del,
};
