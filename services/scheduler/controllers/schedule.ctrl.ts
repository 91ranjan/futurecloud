import { Agenda } from 'agenda/es';
import serverconfig from '../../../config/dev';

const agenda = new Agenda({ db: { address: serverconfig.dbUrl, collection: 'fc_jobs' } });

export async function define(name, cb, opts = {}) {
    return await agenda.define(name, opts, async (job) => {
        await cb(job);
    });
}

export async function schedule(when, name, data) {
    return await agenda.schedule(when, name, data);
}

export async function now(name, data) {
    return await agenda.now(name, data);
}

export async function every(interval, name) {
    return await agenda.every(interval, name);
}

export async function cancel(filters) {
    return await agenda.cancel(filters);
}

export async function start() {
    return await agenda.start();
}
