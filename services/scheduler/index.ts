import * as schedule from './controllers/schedule.ctrl';

export default {
    controllers: { schedule: schedule },
};
